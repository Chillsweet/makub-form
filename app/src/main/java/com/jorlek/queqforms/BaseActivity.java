package com.jorlek.queqforms;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.jorlek.queqforms.custom.DialogCreate;
import com.jorlek.queqforms.login.LoginActivity;
import com.jorlek.queqforms.services.Services;

import rd.TDA.TDA;

/**
 * Created by chillsweet on 4/4/2017 AD.
 */

public class BaseActivity extends AppCompatActivity {

    public Services services;

    public String APP_VERSION;

    public TDA TDA;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TDA = new TDA(this);

        services = new Services(this);

        setAppVersion();

    }

    public void setAppVersion() {
        APP_VERSION = "1.0.0";
    }

    public void nextActivity(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    public void nextActivityForResult(Intent intent, int resultCode) {
        startActivityForResult(intent, resultCode);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    public void previousActivity() {
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    public void startProcess(){
        TDA.serviceTA("0");                                             //Close previous service if exist
        while (TDA.serviceTA("9").compareTo("00") != 0);                //Wait until service closed
        TDA.serviceTA("1");                                   //Start TDAService with “TDASample”
        while (TDA.serviceTA("9").compareTo("01") != 0);                //Wait until service started

        //Check license file
        String check = TDA.infoTA("4");                                 //Test Command
        Log.i("Check", "check = " + check);                             //Print Log

// -2 = INVALID LICENSE
// -12 = LICENSE FILE ERROR
        if (check.compareTo("-2") == 0 || check.compareTo("-12") == 0) {
            if (Global.isNetworkOnline(this)) {                                           //Method Check Internet
                TDA.serviceTA("2");                                     //Update license file
            }
        }
    }
}
