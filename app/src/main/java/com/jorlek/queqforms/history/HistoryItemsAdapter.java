package com.jorlek.queqforms.history;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.AdapterHistoryItemsBinding;
import com.jorlek.queqforms.models.response.M_reqFormHistoryList;

/**
 * Created by ChillSweet on 3/1/2016.
 */
public class HistoryItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private M_reqFormHistoryList mReqFormHistoryList;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private int page_index;

    public HistoryItemsAdapter(Context mContext, M_reqFormHistoryList mReqFormHistoryList) {
        this.mContext = mContext;
        this.mReqFormHistoryList = mReqFormHistoryList;

    }

    public void addAll(M_reqFormHistoryList collection, int page_index) {
        mReqFormHistoryList = collection;
        this.page_index = page_index;
        notifyDataSetChanged();
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        private AdapterHistoryItemsBinding binding;

        private ItemViewHolder(AdapterHistoryItemsBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(M_reqFormHistoryList model, int position) {
            binding.tvName.setText(model.getEmp_form_list().get(position).getEmp_fullname());
            binding.tvDesc.setText(model.getEmp_form_list().get(position).getForm_desc());
            binding.tvDate.setText(model.getEmp_form_list().get(position).getCreated_datetime());
            binding.tvStatus.setText(model.getEmp_form_list().get(position).getForm_state_name());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            AdapterHistoryItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_history_items, parent, false);
            vh = new ItemViewHolder(binding);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            final M_reqFormHistoryList model = mReqFormHistoryList;
            ((ItemViewHolder) holder).bind(model, position);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mReqFormHistoryList.getEmp_form_list().size();
    }

    @Override
    public int getItemViewType(int position) {
        return mReqFormHistoryList.getEmp_form_list().get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }
}
