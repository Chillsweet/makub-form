package com.jorlek.queqforms.history;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;

import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.dynamic.DynamicFormCustom;
import com.jorlek.queqforms.custom.dynamic.DynamicFormHistoryCustom;
import com.jorlek.queqforms.databinding.ActivityHistoryDetailBinding;
import com.jorlek.queqforms.models.response.M_formList;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.models.response.M_reqFormHistoryList;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class HistoryDetailActivity extends BaseActivity {

    ActivityHistoryDetailBinding binding;

    M_formList mFormList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_history_detail);

        getBundleExtra();

        setTopBar();

        setOnClick();

        setHistoryDetail();

    }

    private void getBundleExtra() {
        Bundle extras = getIntent().getExtras();
        mFormList =  extras.getParcelable("history_detail");
    }

    private void setTopBar() {
        binding.includeTopBar.tvTitle.setText(getString(R.string.history_title));
        binding.includeTopBar.btnMenu.setVisibility(View.GONE);
        binding.includeTopBar.btnBack.setVisibility(View.VISIBLE);
    }

    private void setOnClick() {
        binding.includeTopBar.btnBack.setOnClickListener(view -> previousActivity());
    }

    private void setHistoryDetail() {
        binding.tvDetail.setText(mFormList.getForm_detail());
        for (M_formQuestionList formQuestionList : mFormList.getForm_question_list()) {
            binding.containerForm.addView(new DynamicFormHistoryCustom(HistoryDetailActivity.this, formQuestionList));
        }
    }

}
