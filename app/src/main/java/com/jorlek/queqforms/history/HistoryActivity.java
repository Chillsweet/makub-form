package com.jorlek.queqforms.history;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.ActivityHistoryBinding;
import com.jorlek.queqforms.helper.RecyclerItemClickListener;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.request.J_reqFormHistoryList;
import com.jorlek.queqforms.models.response.M_reqFormHistoryList;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.LoadMoreItemListener;
import com.jorlek.queqforms.services.OnLoadMoreListener;
import com.jorlek.queqforms.services.TokenException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class HistoryActivity extends BaseActivity implements OnLoadMoreListener, LoadMoreItemListener {

    ActivityHistoryBinding binding;

    M_reqFormHistoryList mReqFormHistoryList;

    HistoryItemsAdapter historyItemsAdapter;

    private int page_index = 1;
    private int page_size = 10;
    private int page_total;
    private boolean isLoading = false;
    private int visibleThreshold = 1;
    int indexLoading = -1;
    private int lastVisibleItem, totalItemCount;
    private boolean isOnCreate = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_history);

        setTopBar();

        setOnClick();

        callReqFormHistoryList(page_index, page_size, true);

    }

    private void setTopBar() {
        binding.includeTopBar.tvTitle.setText(getString(R.string.history_title));
        binding.includeTopBar.btnBack.setVisibility(View.VISIBLE);
        binding.includeTopBar.btnMenu.setVisibility(View.GONE);
    }

    private void setOnClick() {
        binding.includeTopBar.btnBack.setOnClickListener(view -> previousActivity());
        binding.rvHistory.addOnItemTouchListener(new RecyclerItemClickListener(HistoryActivity.this,
                (view, position) -> {
                    Intent intent = new Intent(HistoryActivity.this, HistoryDetailActivity.class);
                    intent.putExtra("history_detail", mReqFormHistoryList.getEmp_form_list().get(position));
                    nextActivity(intent);
                }));
    }

    private void setHistoryItem(M_reqFormHistoryList mReqFormHistoryList) {
        if (isOnCreate) {
            this.mReqFormHistoryList.getEmp_form_list().addAll(mReqFormHistoryList.getEmp_form_list());
            notifyDataSetChanged(this.mReqFormHistoryList);
        }
        else {
            this.mReqFormHistoryList = mReqFormHistoryList;
            historyItemsAdapter = new HistoryItemsAdapter(this, mReqFormHistoryList);
            binding.rvHistory.setLayoutManager(new LinearLayoutManager(this));
            binding.rvHistory.setAdapter(historyItemsAdapter);
            isOnCreate = true;
        }
    }

    private void callReqFormHistoryList(int page_index, int page_size, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            J_reqFormHistoryList jReqFormHistoryList = new J_reqFormHistoryList(page_index, page_size);
            Observable<M_reqFormHistoryList> callReqFormHistoryListApi = services.callApi.reqFormHistoryList(Prefs.getToken(this), jReqFormHistoryList);
            callReqFormHistoryListApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqFormHistoryList>() {
                        @Override
                        public void onNext(M_reqFormHistoryList mReqFormHistoryList) {
                            if (mReqFormHistoryList != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqFormHistoryList.getResult_code())) {
                                        setHistoryItem(mReqFormHistoryList);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(HistoryActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(HistoryActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqFormHistoryList", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    /**
     * Load More
     */
    @Override
    public void onCallServiceItem(int page_index, String callService) {
        callReqFormHistoryList(page_index, page_size, false);
    }

    public void notifyDataSetChanged(M_reqFormHistoryList mReqFormHistoryList) {
        this.mReqFormHistoryList = mReqFormHistoryList;
        writeLog.LogE("SIZE", "size: " + this.mReqFormHistoryList.getEmp_form_list().size());
        historyItemsAdapter.addAll(this.mReqFormHistoryList, page_index);
        onLoadMoreSuccess();
    }

    @Override
    public void onLoadMore() {
        this.mReqFormHistoryList.getEmp_form_list().add(null);
        historyItemsAdapter.notifyItemInserted(this.mReqFormHistoryList.getEmp_form_list().size() - 1);
        indexLoading = this.mReqFormHistoryList.getEmp_form_list().size();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onCallServiceItem(page_index, "history");
            }
        }, 300);
    }

    public void onLoadMoreSuccess() {
        if (indexLoading != -1) {
            this.mReqFormHistoryList.getEmp_form_list().remove(indexLoading - 1);
            isLoading = false;
            indexLoading = -1;
            historyItemsAdapter.notifyDataSetChanged();
        }
        else {
            if (!isLoading) {
                binding.rvHistory.scrollToPosition(0);
            }
        }
    }

}
