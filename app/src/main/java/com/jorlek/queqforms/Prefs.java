package com.jorlek.queqforms;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ChillSweet on 3/4/2016.
 */
public class Prefs {

    private static final String PREFS = "myprefs";
    private static final String LANGUAGE = "language";
    private static final String COMPANY_CODE = "company_code";
    private static final String USER_ID = "user_id";
    private static final String DEVICE_ID = "device_id";
    private static final String DEVICE_CODE = "device_code";
    private static final String REGISTRATION = "registration";
    private static final String FIRE_BASE_TOKEN = "fire_base_token";
    private static final String KEY_AES1 = "key_aes1";
    private static final String KEY_AES2 = "key_aes2";
    private static final String TOKEN = "token";


    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
//        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setPrefs(Context context, String value) {
        getPrefs(context).edit().putString(PREFS, value).commit();
    }

    public static String getLanguage(Context context) {
        String check = getPrefs(context).getString(LANGUAGE, "th");
        return check;
    }

    public static void setLanguage(Context context, String value) {
        getPrefs(context).edit().putString(LANGUAGE, value).commit();
    }

    public static String getCompanyCode(Context context) {
        String check = getPrefs(context).getString(COMPANY_CODE, null);
        return check;
    }

    public static void setCompanyCode(Context context, String value) {
        getPrefs(context).edit().putString(COMPANY_CODE, value).commit();
    }

    public static String getUserID(Context context) {
        String check = getPrefs(context).getString(USER_ID, null);
        return check;
    }

    public static void setUserID(Context context, String value) {
        getPrefs(context).edit().putString(USER_ID, value).commit();
    }

    public static String getDeviceID(Context context) {
        String check = getPrefs(context).getString(DEVICE_ID, null);
        return check;
    }

    public static void setDeviceID(Context context, String value) {
        getPrefs(context).edit().putString(DEVICE_ID, value).commit();
    }

    public static String getDeviceCode(Context context) {
        String check = getPrefs(context).getString(DEVICE_CODE, null);
        return check;
    }

    public static void setDeviceCode(Context context, String value) {
        getPrefs(context).edit().putString(DEVICE_CODE, value).commit();
    }

    public static String getRegistration(Context context) {
        String check = getPrefs(context).getString(REGISTRATION, "no");
        return check;
    }

    public static void setRegistration(Context context, String value) {
        getPrefs(context).edit().putString(REGISTRATION, value).commit();
    }

    public static String getFireBaseToken(Context context) {
        String check = getPrefs(context).getString(FIRE_BASE_TOKEN, null);
        return check;
    }

    public static void setFireBaseToken(Context context, String value) {
        getPrefs(context).edit().putString(FIRE_BASE_TOKEN, value).commit();
    }

    public static String getKeyAES1(Context context) {
        String check = getPrefs(context).getString(KEY_AES1, null);
        return check;
    }

    public static void setKeyAES1(Context context, String value) {
        getPrefs(context).edit().putString(KEY_AES1, value).commit();
    }

    public static String getKeyAES2(Context context) {
        String check = getPrefs(context).getString(KEY_AES2, null);
        return check;
    }

    public static void setKeyAES2(Context context, String value) {
        getPrefs(context).edit().putString(KEY_AES2, value).commit();
    }

    public static String getToken(Context context) {
        String check = getPrefs(context).getString(TOKEN, null);
        return check;
    }

    public static void setToken(Context context, String value) {
        getPrefs(context).edit().putString(TOKEN, value).commit();
    }

}
