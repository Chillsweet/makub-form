package com.jorlek.queqforms.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.gson.Gson;
import com.jorlek.queqforms.AppConfig;
import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.DialogConfirm;
import com.jorlek.queqforms.custom.DialogCreate;
import com.jorlek.queqforms.custom.DialogDub;
import com.jorlek.queqforms.custom.DialogSuccess;
import com.jorlek.queqforms.custom.dynamic.DynamicFormCustom;
import com.jorlek.queqforms.databinding.ActivityMainBinding;
import com.jorlek.queqforms.history.HistoryActivity;
import com.jorlek.queqforms.login.LoginActivity;
import com.jorlek.queqforms.models.request.J_Form_Question;
import com.jorlek.queqforms.models.request.J_reqCheckCarddub;
import com.jorlek.queqforms.models.request.J_reqCreateFormByEmp;
import com.jorlek.queqforms.models.request.J_reqFormRequestList;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.models.response.M_formRequestList;
import com.jorlek.queqforms.models.response.M_reqCheckDub;
import com.jorlek.queqforms.models.response.M_reqCreateFormByEmp;
import com.jorlek.queqforms.models.response.M_reqFormRequestList;
import com.jorlek.queqforms.models.response.M_reqModelGift;
import com.jorlek.queqforms.models.response.M_uploadFile;
import com.jorlek.queqforms.report.ReportActivity;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.TokenException;
import com.jorlek.queqforms.helper.writeLog;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rd.TDA.TDA;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ActivityMainBinding binding;

    private M_reqFormRequestList mReqFormRequestList;

    private static M_reqModelGift mReqModelGift;

    private M_formRequestList formRequestList;

    private ArrayList<J_Form_Question> jFormQuestionsList;

    private J_Form_Question jFormQuestion;

    private Handler handler = new Handler();

    boolean isMenuVisible = false;
    boolean requireField = true;
    boolean isGiftAdd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        jFormQuestionsList = new ArrayList<>();

        setTopBar();

        setOnClick();

        callReqFormRequestList(true);

    }

    private void setTopBar() {
        binding.includeTopBar.tvTitle.setText(getString(R.string.main_title));
        binding.includeTopBar.btnScan.setVisibility(View.VISIBLE);
    }

    private void setOnClick() {
        binding.includeTopBar.btnMenu.setOnClickListener(this);
        binding.includeTopBar.btnScan.setOnClickListener(this);
        binding.rlMenu.setOnClickListener(this);
        binding.includeMenu.btnHistory.setOnClickListener(this);
        binding.includeMenu.btnReport.setOnClickListener(this);
        binding.includeMenu.btnLogout.setOnClickListener(this);
        binding.btnSend.setOnClickListener(this);
        binding.btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_scan:
                doBtnScanIDCard();
                break;

            case R.id.btn_menu:
                doBtnMenu();
                break;

            case R.id.btn_history:
                doBtnHistory();
                break;

            case R.id.btn_report:
                doBtnReport();
                break;

            case R.id.btn_logout:
                doBtnLogout();
                break;

            case R.id.btn_send:
                uploadSignature();
                break;

            case R.id.btn_delete:
                doBtnDelete();
                break;
        }
    }

    private void doBtnScanIDCard() {
        binding.includeTopBar.btnScan.setClickable(false);
        Thread thread = new Thread(new Runnable() {
            String Data;

            @Override
            public void run() {

                //clear Screen
                handler.post(() -> {
                    for (int i = 0; i < 3; i++) {
                        DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
                        dynamicFormCustom.getEditText().setText("");
                        dynamicFormCustom.setInput("");
                    }
                });

                //Read Text from NID card
                Data = TDA.nidTextTA("0");                      //ReadText
                if (Data.compareTo("-2") == 0) {                //Check if un-registered reader
                    TDA.serviceTA("2");                         //Update license file
                    Data = TDA.nidTextTA("0");                  //Read Text Again
                }

                handler.post(() -> {
                    switch (Data) {
                        case "-3":
                            new DialogCreate(MainActivity.this).AlertError(getString(R.string.dialog_id_card_3),
                                    getString(R.string.check_expire_button), (dialogInterface, k) -> {
                                    });
                            binding.includeTopBar.btnScan.setClickable(true);
                            break;

                        case "-4":
                            new DialogCreate(MainActivity.this).AlertError(getString(R.string.dialog_id_card_4),
                                    getString(R.string.check_expire_button), (dialogInterface, k) -> {
                                    });
                            binding.includeTopBar.btnScan.setClickable(true);
                            break;

                        case "-16":
                            new DialogCreate(MainActivity.this).AlertError(getString(R.string.dialog_id_card_16),
                                    getString(R.string.check_expire_button), (dialogInterface, k) -> {
                                    });
                            binding.includeTopBar.btnScan.setClickable(true);
                            break;

                        default:
                            String idCard[] = Data.split("#");
                            for (int i = 0; i < 5; i++) {
                                DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
                                switch (i) {
                                    //first Name
                                    case 0:
                                        dynamicFormCustom.getEditText().setText(idCard[2]);
                                        dynamicFormCustom.setInput(idCard[2]);
                                        break;

                                    //last Name
                                    case 1:
                                        dynamicFormCustom.getEditText().setText(idCard[4]);
                                        dynamicFormCustom.setInput(idCard[4]);
                                        break;

                                    //id number
                                    case 2:
                                        dynamicFormCustom.getEditText().setText(idCard[0]);
                                        dynamicFormCustom.setInput(idCard[0]);
                                        break;

                                    //address
                                    case 3:
                                        String address = "";
                                        for (int j = 9; j < 17; j++) {
                                            if (j == 9) address = idCard[j];
                                            else if (!idCard[j].equals(""))
                                                address = address + " " + idCard[j];
                                        }
                                        dynamicFormCustom.getTextView().setText(address);
                                        dynamicFormCustom.setInput(address);
                                        break;

                                    //birth date
                                    case 4:
                                        dynamicFormCustom.getTextView().setText(idCard[18]);
                                        dynamicFormCustom.setInput(idCard[18]);
                                        break;
                                }
                            }
                            callReqCheckIdCarddub(idCard[0], true);
                            binding.includeTopBar.btnScan.setClickable(true);
                            break;
                    }
                });
            }
        });
        thread.start();
    }

    private void doBtnHistory() {
        Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
        nextActivity(intent);
    }

    private void doBtnReport() {
        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
        nextActivity(intent);
    }

    private void doBtnLogout() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        nextActivity(intent);
    }

    private void doBtnMenu() {
        if (isMenuVisible) {
            isMenuVisible = false;
            binding.includeTopBar.btnMenu.setImageResource(R.drawable.ic_menu);
            slideMenuUp();
        }
        else {
            isMenuVisible = true;
            binding.includeTopBar.btnMenu.setImageResource(R.drawable.close_menu);
            slideMenuDown();
        }
    }

    private void doBtnDelete() {
        DialogConfirm dialogConfirm = new DialogConfirm(this);
        dialogConfirm.showDialog(view -> {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            finish();
            nextActivity(intent);
        });
    }

    private void slideMenuUp() {
        binding.rlMenu.animate()
                .translationY(-binding.rlMenu.getHeight())
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.rlMenu.setVisibility(View.GONE);
                    }
                });
    }

    private void slideMenuDown() {
        binding.rlMenu.animate()
                .translationY(0)
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.rlMenu.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConfig.SELECT_STORE:
                    String form_question_id = data.getStringExtra("form_question_id");
                    String name = data.getStringExtra("name");
                    String val = data.getStringExtra("val");
                    setRadioNewPage(form_question_id, name, val);
                    break;
            }
        }
    }

    private void setRadioNewPage(String form_question_id, String name, String val) {
        for (int i = 0; i < binding.containerForm.getChildCount(); i++) {
            DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
            if (dynamicFormCustom.getQuestionID().equals(form_question_id)) {
                dynamicFormCustom.getTextView().setText(name);
                dynamicFormCustom.setName(name);
                dynamicFormCustom.setVal(val);
                break;
            }
        }
    }

    private void uploadSignature() {
        DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(binding.containerForm.getChildCount() - 1);
        if (dynamicFormCustom.isSign()) {
            File fileUpload = new File("/data/data/" + getPackageName() + "/sign.png");
            Bitmap bitmap = Global.getBitmapResize(MainActivity.this, fileUpload.getPath());
            String pathNewImage = Global.createImageFile(MainActivity.this, "uploadFile.jpg").getPath();
            Global.writeImageFile(bitmap, pathNewImage);
            callUploadFile(dynamicFormCustom.getQuestionID(), pathNewImage, true);
        }
        else
            new DialogCreate(this).AlertError(getString(R.string.main_filled), getString(R.string.check_expire_button), (dialogInterface, k) -> {
            });

    }

    private void doBtnSend() {
        for (int i = 0; i < binding.containerForm.getChildCount(); i++) {
            DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
            switch (dynamicFormCustom.getQuestionType()) {
                case 3: //drop down
                case 7: //radio
                case 12: //radio textview
                    jFormQuestion = new J_Form_Question();
                    jFormQuestion.setForm_question_id(dynamicFormCustom.getQuestionID());
                    jFormQuestion.setName(dynamicFormCustom.getName());
                    jFormQuestion.setVal(dynamicFormCustom.getVal());
                    jFormQuestionsList.add(jFormQuestion);
                    if (dynamicFormCustom.isRequired()) {
                        if (Global.isStringEmpty(dynamicFormCustom.getName()) && Global.isStringEmpty(dynamicFormCustom.getVal())) {
                            writeLog.LogE("doBtnSend", dynamicFormCustom.getTitle());
                            requireField = false;
                        }
                    }
                    break;

                case 8: //check box
                    for (int j = 0; j < dynamicFormCustom.getDynamicFieldCustomArrayList().size(); j++) {
                        jFormQuestion = new J_Form_Question();
                        jFormQuestion.setForm_question_id(dynamicFormCustom.getQuestionID());
                        jFormQuestion.setName(dynamicFormCustom.getDynamicFieldCustomArrayList().get(j).getName());
                        jFormQuestion.setVal(dynamicFormCustom.getDynamicFieldCustomArrayList().get(j).getVal());
                        jFormQuestionsList.add(jFormQuestion);
                        if (dynamicFormCustom.isRequired()) {
                            if (Global.isStringEmpty(dynamicFormCustom.getName()) && Global.isStringEmpty(dynamicFormCustom.getVal())) {
                                writeLog.LogE("doBtnSend", dynamicFormCustom.getTitle());
                                requireField = false;
                            }
                        }
                    }
                    break;

                case 9: //Signature
                    break;

                default:
                    jFormQuestion = new J_Form_Question();
                    jFormQuestion.setForm_question_id(dynamicFormCustom.getQuestionID());
                    jFormQuestion.setName(dynamicFormCustom.getInput());
                    jFormQuestion.setVal(dynamicFormCustom.getInput());
                    jFormQuestionsList.add(jFormQuestion);
                    if (dynamicFormCustom.isRequired()) {
                        if (Global.isStringEmpty(dynamicFormCustom.getInput())) {
                            writeLog.LogE("doBtnSend", dynamicFormCustom.getTitle());
                            requireField = false;
                        }
                    }
                    break;

            }
        }
        writeLog.LogE("btn_send", new Gson().toJson(jFormQuestionsList));
        if (!requireField)
            new DialogCreate(this).AlertError(getString(R.string.main_filled), getString(R.string.check_expire_button), (dialogInterface, i) -> {
            });
        else {
            callReqCreateFormByEmp(formRequestList.getForm_id(), new Gson().toJson(jFormQuestionsList), true);
        }
    }

    private void callReqFormRequestList(boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            J_reqFormRequestList jReqFormRequestList = new J_reqFormRequestList("");
            Observable<M_reqFormRequestList> callReqFormRequestListApi = services.callApi.reqFormRequestList(Prefs.getToken(MainActivity.this), jReqFormRequestList);
            callReqFormRequestListApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqFormRequestList>() {
                        @Override
                        public void onNext(M_reqFormRequestList mReqFormRequestList) {
                            if (mReqFormRequestList != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqFormRequestList.getResult_code())) {
                                        setQuestionList(mReqFormRequestList);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(MainActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(MainActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqFormRequestList", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private void setQuestionList(M_reqFormRequestList mReqFormRequestList) {
        this.mReqFormRequestList = mReqFormRequestList;
        this.formRequestList = mReqFormRequestList.getForm_list().get(0);
        for (M_formQuestionList formQuestionList : mReqFormRequestList.getForm_list().get(0).getForm_question()) {
            binding.containerForm.addView(new DynamicFormCustom(MainActivity.this, formQuestionList));
        }
        setBtnSend();
    }

    public void setGiftList(M_reqModelGift mReqModelGift) {
        if (isGiftAdd) {
            binding.containerForm.removeViewAt(binding.containerForm.getChildCount() - 1);
        }
        binding.containerForm.removeViewAt(binding.containerForm.getChildCount() - 1);
        binding.containerForm.addView(new DynamicFormCustom(MainActivity.this, mReqModelGift.getFormQuestionList()));
        int lastViewPosition = mReqFormRequestList.getForm_list().get(0).getForm_question().size() - 1;
        binding.containerForm.addView(new DynamicFormCustom(MainActivity.this, mReqFormRequestList.getForm_list().get(0).getForm_question().get(lastViewPosition)));
        isGiftAdd = true;
    }

    private void setBtnSend() {
        binding.llBtnSend.setVisibility(View.VISIBLE);
        binding.tvRemark.setText(formRequestList.getForm_remark());
    }

    private void callReqCheckIdCarddub(String id_card, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            J_reqCheckCarddub jReqCheckCarddub = new J_reqCheckCarddub(id_card);
            Observable<M_reqCheckDub> callReqCheckIdCarddubApi = services.callApi.reqCheckIdCarddub(Prefs.getToken(MainActivity.this), jReqCheckCarddub);
            callReqCheckIdCarddubApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqCheckDub>() {
                        @Override
                        public void onNext(M_reqCheckDub mReqCheckDub) {
                            if (mReqCheckDub != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqCheckDub.getResult_code())) {
                                        checkCarddub(mReqCheckDub);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(MainActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(MainActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqCheckIdCarddub", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private void checkCarddub(M_reqCheckDub mReqCheckDub) {
        if (mReqCheckDub.isDub_flag()) {
            DialogDub dialogDub = new DialogDub(MainActivity.this, mReqCheckDub);
            dialogDub.showDialog();
//            for (int i = 0; i < 3; i++) {
//                DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
//                dynamicFormCustom.getEditText().setText("");
//            }
        }
    }

    private void callUploadFile(String form_question_id, String image_path, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            File file = new File(image_path);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpg"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "file_content");
            Observable<M_uploadFile> callUploadFileApi = services.callApi.uploadFile(Prefs.getToken(MainActivity.this), body, name);
            callUploadFileApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_uploadFile>() {
                        @Override
                        public void onNext(M_uploadFile mUploadFile) {
                            if (mUploadFile != null) {
                                try {
                                    if (CheckResult.checkSuccess(mUploadFile.getResult_code())) {
                                        jFormQuestion = new J_Form_Question();
                                        jFormQuestion.setForm_question_id(form_question_id);
                                        jFormQuestion.setName(mUploadFile.getFile_data().getSave_path());
                                        jFormQuestion.setVal(mUploadFile.getFile_data().getSave_path());
                                        jFormQuestionsList.add(jFormQuestion);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(MainActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(MainActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callUploadFile", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                            doBtnSend();
                        }
                    });
        }

    }

    private void callReqCreateFormByEmp(String form_id, String emp_form_value, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            J_reqCreateFormByEmp jReqCreateFormByEmp = new J_reqCreateFormByEmp(form_id, emp_form_value);
            Observable<M_reqCreateFormByEmp> callReqCreateFormByEmpApi = services.callApi.reqCreateFormByEmp(Prefs.getToken(MainActivity.this), jReqCreateFormByEmp);
            callReqCreateFormByEmpApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqCreateFormByEmp>() {
                        @Override
                        public void onNext(M_reqCreateFormByEmp mReqCreateFormByEmp) {
                            if (mReqCreateFormByEmp != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqCreateFormByEmp.getResult_code())) {
                                        checkCreateForm(mReqCreateFormByEmp);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(MainActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(MainActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqCreateFormByEmp", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private void checkCreateForm(M_reqCreateFormByEmp mReqCreateFormByEmp) {
        if (mReqCreateFormByEmp.isPass_flag()) {
            DialogSuccess dialogSuccess = new DialogSuccess(this, mReqCreateFormByEmp);
            dialogSuccess.showDialog(view -> {
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                finish();
                nextActivity(intent);
            });
        }
        else {
            new DialogCreate(this).AlertError(mReqCreateFormByEmp.getPass_message(),
                    getString(R.string.check_expire_button), (dialogInterface, k) -> {
                    });
        }
    }

}
