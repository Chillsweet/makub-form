package com.jorlek.queqforms.registration;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.DialogCreate;
import com.jorlek.queqforms.databinding.ActivityRegistrationBinding;
import com.jorlek.queqforms.login.LoginActivity;
import com.jorlek.queqforms.models.request.J_reqDefaultInfo;
import com.jorlek.queqforms.models.response.M_reqDefaultInfo;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.TokenException;
import com.jorlek.queqforms.helper.writeLog;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by chillsweet on 4/4/2017 AD.
 */

public class RegistrationActivity extends BaseActivity implements View.OnClickListener {

    ActivityRegistrationBinding binding;

    String companyId, userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration);

        setOnClick();

        Global.getAboutDevice(this);

    }

    private void setOnClick() {
        binding.btnLogin.setOnClickListener(this);
    }

    private boolean isFieldEmpty() {
        companyId = binding.edtCompanyId.getText().toString();
        userId = binding.edtUserId.getText().toString();
        if (!Global.isStringEmpty(companyId) && !Global.isStringEmpty(userId))
            return false;
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (!isFieldEmpty()) callReqDefaultInfo(Global.DEVICE_ID, Prefs.getLanguage(this), companyId, userId, true);
                break;
        }
    }

    private void callReqDefaultInfo(String deviceId, String language, String companyId, String userId, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            Prefs.setFireBaseToken(RegistrationActivity.this, FirebaseInstanceId.getInstance().getToken());
            J_reqDefaultInfo jReqDefaultInfo = new J_reqDefaultInfo(deviceId, Prefs.getFireBaseToken(this), language);
            Observable<M_reqDefaultInfo> callReqDefaultInfoApi = services.callApi.reqDefaultInfo(companyId, userId, jReqDefaultInfo);
            callReqDefaultInfoApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqDefaultInfo>() {
                        @Override
                        public void onNext(M_reqDefaultInfo mReqDefaultInfo) {
                            if(mReqDefaultInfo != null) {
                                try {
                                    if(CheckResult.checkSuccess(mReqDefaultInfo.getResult_code())) {
                                        Prefs.setCompanyCode(RegistrationActivity.this, companyId);
                                        Prefs.setUserID(RegistrationActivity.this, userId);
                                        Prefs.setDeviceID(RegistrationActivity.this, mReqDefaultInfo.getDevice_id());
                                        Prefs.setDeviceCode(RegistrationActivity.this, mReqDefaultInfo.getDevice_code());
                                        Prefs.setKeyAES1(RegistrationActivity.this,  mReqDefaultInfo.getKey_aes());
                                        Prefs.setRegistration(RegistrationActivity.this, "yes");
                                        if(mReqDefaultInfo.isDelete_flag())
                                            reqDefaultInfoDeleteFlag(mReqDefaultInfo);
                                        else
                                            reqDefaultInfoSuccess(mReqDefaultInfo);
                                    }
                                    else {
                                        reqDefaultInfoInvalid(mReqDefaultInfo);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(RegistrationActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(RegistrationActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqDefaultInfo", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private void reqDefaultInfoDeleteFlag(M_reqDefaultInfo mReqDefaultInfo) {
        new DialogCreate(this).AlertError(mReqDefaultInfo.getDelete_message(), getString(R.string.check_expire_button),
                (dialog, which) -> {
                    Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                    i.putExtra("key_aes", mReqDefaultInfo.getKey_aes());
                    i.putExtra("first_flag", mReqDefaultInfo.isFirst_flag());
                    nextActivity(i);
                    finish();
                });
    }

    private void reqDefaultInfoInvalid(M_reqDefaultInfo mReqDefaultInfo) {
        if (mReqDefaultInfo.isDelete_flag() && !Global.isStringEmpty(mReqDefaultInfo.getDelete_message())) {
            new DialogCreate(this).AlertError(mReqDefaultInfo.getDelete_message(), getString(R.string.check_expire_button),
                    (dialog, which) -> {});
        }
        binding.edtCompanyId.setText("");
        binding.edtUserId.setText("");
        binding.tvIncorrect.setVisibility(View.VISIBLE);
    }

    private void reqDefaultInfoSuccess(M_reqDefaultInfo mReqDefaultInfo) {
        Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
        i.putExtra("key_aes", mReqDefaultInfo.getKey_aes());
        i.putExtra("first_flag", mReqDefaultInfo.isFirst_flag());
        nextActivity(i);
        finish();
    }
}
