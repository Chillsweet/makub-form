package com.jorlek.queqforms.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.login.LoginActivity;

/**
 * Created by chillsweet on 4/10/2017 AD.
 */

public class SplashScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Prefs.getRegistration(SplashScreenActivity.this).equals("no")) {
            Intent intent = new Intent(SplashScreenActivity.this, RegistrationActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }
        else {
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }

    }
}
