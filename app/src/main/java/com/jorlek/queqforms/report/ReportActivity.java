package com.jorlek.queqforms.report;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.ActivityReportBinding;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.request.J_sendMailReportFormList;
import com.jorlek.queqforms.models.response.M_sendMailReportFormList;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.TokenException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chillsweet on 4/25/2017 AD.
 */

public class ReportActivity extends BaseActivity {

    ActivityReportBinding binding;

    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;

    private int selectDay, selectMonth, selectYear;
    private int selectHour, selectMinute;
    public int fromDay, fromMonth, fromYear, toDay, toMonth, toYear, fromHrs, fromMin, toHrs, toMin;
    private String fromDate = "", fromTime = "", fromDateTime = "", toDate = "", toTime = "", toDateTime = "";
    private boolean isFromDate = false, isFromTime = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_report);

        setTopBar();

        setCalendar();

        setDatePickerDialog();

        setTimePickerDialog();

        setBoxReport();

        setOnClick();

    }

    private void setTopBar() {
        binding.includeTopBar.tvTitle.setText(getString(R.string.report_title));
        binding.includeTopBar.btnBack.setVisibility(View.VISIBLE);
        binding.includeTopBar.btnMenu.setVisibility(View.GONE);
    }

    private void setBoxReport() {
        binding.includeReportFrom.tvHead.setText(getString(R.string.report_from));
        binding.includeReportTo.tvHead.setText(getString(R.string.report_to));
    }

    private void setCalendar() {
        Calendar cal = Calendar.getInstance();
        selectHour = cal.get(Calendar.HOUR_OF_DAY);
        selectMinute = cal.get(Calendar.MINUTE);
        selectDay = cal.get(Calendar.DAY_OF_MONTH);
        selectMonth = cal.get(Calendar.MONTH);
        selectYear = cal.get(Calendar.YEAR);
    }

    private void setDatePickerDialog() {
        datePickerDialog = new DatePickerDialog(ReportActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int yearOfYear, int monthOfYear, int dayOfYear) {
                selectDay = dayOfYear;
                selectMonth = monthOfYear;
                selectYear = yearOfYear;
                setDate();
            }
        }, selectYear, selectMonth, selectDay);
    }

    private void setDate() {
        String date = Global.buildDateFormat(selectDay, selectMonth, selectYear, Prefs.getLanguage(ReportActivity.this));
        if (isFromDate) {
            fromDate = date;
            fromDay = selectDay;
            fromMonth = selectMonth;
            fromYear = selectYear;
            if (!Global.isStringEmpty(toDate)) checkToDate();
            binding.includeReportFrom.tvDate.setText(fromDate);
        }
        else {
            toDate = date;
            toDay = selectDay;
            toMonth = selectMonth;
            toYear = selectYear;
            checkToDate();
            binding.includeReportTo.tvDate.setText(toDate);
        }
    }

    private void checkToDate() {
        Calendar fromCal = Calendar.getInstance();
        fromCal.set(Calendar.DATE, fromDay);
        fromCal.set(Calendar.MONTH, fromMonth);
        fromCal.set(Calendar.YEAR, fromYear);
        Calendar toCal = Calendar.getInstance();
        toCal.set(Calendar.DATE, toDay);
        toCal.set(Calendar.MONTH, toMonth);
        toCal.set(Calendar.YEAR, toYear);
        if (!toCal.after(fromCal)) {
            toDay = fromDay;
            toMonth = fromMonth;
            toYear = fromYear;
            selectDay = fromDay;
            selectMonth = fromMonth;
            selectYear = fromYear;
            toDate = Global.buildDateFormat(selectDay, selectMonth, selectYear, Prefs.getLanguage(ReportActivity.this));
            binding.includeReportTo.tvDate.setText(toDate);
        }
        if (!Global.isStringEmpty(fromTime) && !Global.isStringEmpty(toTime)) checkToTime();
    }

    private void setTimePickerDialog() {
        timePickerDialog = new TimePickerDialog(ReportActivity.this, TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                selectHour = hour;
                selectMinute = minute;
                setTime();
            }
        }, selectHour, selectMinute, true);
        timePickerDialog.setTitle("");
    }

    private void setTime() {
        String time = Global.buildTimeFormat(selectHour, selectMinute);
        if (isFromTime) {
            fromTime = time;
            fromHrs = selectHour;
            fromMin = selectMinute;
            if (!Global.isStringEmpty(toTime)) checkFromTime();
            binding.includeReportFrom.tvTime.setText(fromTime);
        }
        else {
            toTime = time;
            toHrs = selectHour;
            toMin = selectMinute;
            checkToTime();
            binding.includeReportTo.tvTime.setText(toTime);
        }
    }

    private void checkFromTime() {
        SimpleDateFormat sdtf = new SimpleDateFormat("yyyy:MM:dd:HH:mm", Locale.US);
        SimpleDateFormat stf = new SimpleDateFormat("HH:mm", Locale.US);
        try {
            Date fromDateParse = sdtf.parse(fromYear + ":" + fromMonth + ":" + fromDay + ":" + fromHrs + ":" + fromMin);
            Date toDateParse = sdtf.parse(toYear + ":" + toMonth + ":" + toDay + ":" + toHrs + ":" + toMin);
            if (!toDateParse.after(fromDateParse)) {
                String time = fromHrs + ":" + fromMin;
                Calendar c = Calendar.getInstance();
                c.setTime(stf.parse(time));
                c.add(Calendar.HOUR, 1);
                time = stf.format(c.getTime());
                toTime = time;
                binding.includeReportTo.tvTime.setText(toTime);
                if (fromHrs == 23) {
                    String date = fromDay + " " + (fromMonth + 1) + " " + fromYear;
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy", Locale.US);
                    Calendar cd = Calendar.getInstance();
                    cd.setTime(sdf.parse(date));
                    cd.add(Calendar.DAY_OF_YEAR, 1);
                    date = sdf.format(cd.getTime());
                    String[] dateFormat = date.split(" ");
                    toDay = Integer.parseInt(dateFormat[0]);
                    toMonth = (Integer.parseInt(dateFormat[1]) - 1) % 12;
                    toYear = Integer.parseInt(dateFormat[2]);
                    toDate = Global.buildDateFormat(toDay, toMonth, toYear, Prefs.getLanguage(ReportActivity.this));
                    binding.includeReportTo.tvDate.setText(toDate);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void checkToTime() {
        SimpleDateFormat sdtf = new SimpleDateFormat("yyyy:MM:dd:HH:mm", Locale.US);
        try {
            Date fromDateParse = sdtf.parse(fromYear + ":" + fromMonth + ":" + fromDay + ":" + fromHrs + ":" + fromMin);
            Date toDateParse = sdtf.parse(toYear + ":" + toMonth + ":" + toDay + ":" + toHrs + ":" + toMin);
            if (!toDateParse.after(fromDateParse)) {
                String date = fromDay + " " + (fromMonth + 1) + " " + fromYear;
                SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy", Locale.US);
                Calendar cd = Calendar.getInstance();
                cd.setTime(sdf.parse(date));
                cd.add(Calendar.DAY_OF_YEAR, 1);
                date = sdf.format(cd.getTime());
                String[] dateFormat = date.split(" ");
                toDay = Integer.parseInt(dateFormat[0]);
                toMonth = (Integer.parseInt(dateFormat[1]) - 1) % 12;
                toYear = Integer.parseInt(dateFormat[2]);
                toDate = Global.buildDateFormat(toDay, toMonth, toYear, Prefs.getLanguage(ReportActivity.this));
                binding.includeReportTo.tvDate.setText(toDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setOnClick() {
        binding.includeReportFrom.btnDate.setOnClickListener(view -> {
            isFromDate = true;
            datePickerDialog.show();
        });

        binding.includeReportFrom.btnTime.setOnClickListener(view -> {
            isFromTime = true;
            timePickerDialog.show();
        });

        binding.includeReportTo.btnDate.setOnClickListener(view -> {
            isFromDate = false;
            datePickerDialog.show();
        });

        binding.includeReportTo.btnTime.setOnClickListener(view -> {
            isFromTime = false;
            timePickerDialog.show();
        });

        binding.includeTopBar.btnBack.setOnClickListener(view -> previousActivity());

        binding.btnSubmit.setOnClickListener(view -> {
            fromDateTime = fromDate + " " + fromTime + ":00";
            toDateTime = toDate + " " + toTime + ":00";
            callSendMailReportFormList(true);
        });
    }

    private void callSendMailReportFormList(boolean isShowDialog) {
        if (Global.isNetworkOnline(ReportActivity.this)) {
            services.showDialog(isShowDialog);
            J_sendMailReportFormList jSendMailReportFormList = new J_sendMailReportFormList(fromDateTime.trim(), toDateTime.trim(), 1, "");
            Observable<M_sendMailReportFormList> callSendMailReportFormListApi = services.callApi.sendMailReportFormList(Prefs.getToken(ReportActivity.this), jSendMailReportFormList);
            callSendMailReportFormListApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_sendMailReportFormList>() {
                        @Override
                        public void onNext(M_sendMailReportFormList mSendMailReportFormList) {
                            if (mSendMailReportFormList != null) {
                                try {
                                    if (CheckResult.checkSuccess(mSendMailReportFormList.getResult_code())) {
                                        previousActivity();
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(ReportActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(ReportActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callSendMailReportFormList", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

}
