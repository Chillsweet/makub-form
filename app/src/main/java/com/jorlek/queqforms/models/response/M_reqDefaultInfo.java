package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_reqDefaultInfo implements Parcelable {

    private String result_code;
    private String result_desc;
    private String device_id; //(string, optional): device_id ,
    private String device_code; //(string, optional): รหัสเครื่อง ,
    private String key_aes; //(string, optional): key เข้ารหัส ,
    private boolean delete_flag; //(boolean, optional): ถ้าเป็น true ให้เอา delete_message มาโชว์ pop up ,
    private String delete_message; //(string, optional): delete_message ,
    private boolean first_flag; //(boolean, optional): true = login ครั้งแรก

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getDevice_code() {
        return device_code;
    }

    public String getKey_aes() {
        return key_aes;
    }

    public boolean isDelete_flag() {
        return delete_flag;
    }

    public String getDelete_message() {
        return delete_message;
    }

    public boolean isFirst_flag() {
        return first_flag;
    }

    protected M_reqDefaultInfo(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        device_id = in.readString();
        device_code = in.readString();
        key_aes = in.readString();
        delete_flag = in.readByte() != 0;
        delete_message = in.readString();
        first_flag = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result_code);
        dest.writeString(result_desc);
        dest.writeString(device_id);
        dest.writeString(device_code);
        dest.writeString(key_aes);
        dest.writeByte((byte) (delete_flag ? 1 : 0));
        dest.writeString(delete_message);
        dest.writeByte((byte) (first_flag ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_reqDefaultInfo> CREATOR = new Creator<M_reqDefaultInfo>() {
        @Override
        public M_reqDefaultInfo createFromParcel(Parcel in) {
            return new M_reqDefaultInfo(in);
        }

        @Override
        public M_reqDefaultInfo[] newArray(int size) {
            return new M_reqDefaultInfo[size];
        }
    };
}
