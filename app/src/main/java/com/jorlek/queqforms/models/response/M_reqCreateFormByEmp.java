package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_reqCreateFormByEmp implements Parcelable{

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private boolean pass_flag; //(boolean, optional): false = error show pass_message ,
    private String pass_message; //(string, optional): pass_message

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public boolean isPass_flag() {
        return pass_flag;
    }

    public String getPass_message() {
        return pass_message;
    }

    protected M_reqCreateFormByEmp(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        pass_flag = in.readByte() != 0;
        pass_message = in.readString();
    }

    public static final Creator<M_reqCreateFormByEmp> CREATOR = new Creator<M_reqCreateFormByEmp>() {
        @Override
        public M_reqCreateFormByEmp createFromParcel(Parcel in) {
            return new M_reqCreateFormByEmp(in);
        }

        @Override
        public M_reqCreateFormByEmp[] newArray(int size) {
            return new M_reqCreateFormByEmp[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeByte((byte) (pass_flag ? 1 : 0));
        parcel.writeString(pass_message);
    }
}
