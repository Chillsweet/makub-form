package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqCheckCarddub implements Parcelable {

    private String id_card; //(string): IMEI Customer

    public J_reqCheckCarddub(String id_card) {
        this.id_card = id_card;
    }

    protected J_reqCheckCarddub(Parcel in) {
        id_card = in.readString();
    }

    public static final Creator<J_reqCheckCarddub> CREATOR = new Creator<J_reqCheckCarddub>() {
        @Override
        public J_reqCheckCarddub createFromParcel(Parcel in) {
            return new J_reqCheckCarddub(in);
        }

        @Override
        public J_reqCheckCarddub[] newArray(int size) {
            return new J_reqCheckCarddub[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_card);
    }
}
