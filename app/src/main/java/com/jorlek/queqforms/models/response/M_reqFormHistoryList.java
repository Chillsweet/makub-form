package com.jorlek.queqforms.models.response;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_reqFormHistoryList implements Parcelable{

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private ArrayList<M_formList> emp_form_list = new ArrayList<>(); //(Array[formList], optional)
    private int row_total; //(integer, optional): row_total ,
    private int page_index; //(integer, optional): page number ,
    private int page_size; //(integer, optional): page size ,
    private int page_total; //(integer, optional): page total

    protected M_reqFormHistoryList(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        emp_form_list = in.createTypedArrayList(M_formList.CREATOR);
        row_total = in.readInt();
        page_index = in.readInt();
        page_size = in.readInt();
        page_total = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result_code);
        dest.writeString(result_desc);
        dest.writeTypedList(emp_form_list);
        dest.writeInt(row_total);
        dest.writeInt(page_index);
        dest.writeInt(page_size);
        dest.writeInt(page_total);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_reqFormHistoryList> CREATOR = new Creator<M_reqFormHistoryList>() {
        @Override
        public M_reqFormHistoryList createFromParcel(Parcel in) {
            return new M_reqFormHistoryList(in);
        }

        @Override
        public M_reqFormHistoryList[] newArray(int size) {
            return new M_reqFormHistoryList[size];
        }
    };

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public ArrayList<M_formList> getEmp_form_list() {
        return emp_form_list;
    }

    public int getRow_total() {
        return row_total;
    }

    public int getPage_index() {
        return page_index;
    }

    public int getPage_size() {
        return page_size;
    }

    public int getPage_total() {
        return page_total;
    }
}
