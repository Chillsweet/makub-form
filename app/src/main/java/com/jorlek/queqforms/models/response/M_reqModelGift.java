package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/26/2017 AD.
 */

public class M_reqModelGift implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private M_formQuestionList formQuestionList; //(formModelGift, optional)

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public M_formQuestionList getFormQuestionList() {
        return formQuestionList;
    }

    protected M_reqModelGift(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        formQuestionList = in.readParcelable(M_formQuestionList.class.getClassLoader());
    }

    public static final Creator<M_reqModelGift> CREATOR = new Creator<M_reqModelGift>() {
        @Override
        public M_reqModelGift createFromParcel(Parcel in) {
            return new M_reqModelGift(in);
        }

        @Override
        public M_reqModelGift[] newArray(int size) {
            return new M_reqModelGift[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeParcelable(formQuestionList, i);
    }
}
