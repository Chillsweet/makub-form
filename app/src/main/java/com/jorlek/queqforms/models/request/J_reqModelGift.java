package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/26/2017 AD.
 */

public class J_reqModelGift implements Parcelable {

    private String model_name;

    public J_reqModelGift(String model_name) {
        this.model_name = model_name;
    }

    protected J_reqModelGift(Parcel in) {
        model_name = in.readString();
    }

    public static final Creator<J_reqModelGift> CREATOR = new Creator<J_reqModelGift>() {
        @Override
        public J_reqModelGift createFromParcel(Parcel in) {
            return new J_reqModelGift(in);
        }

        @Override
        public J_reqModelGift[] newArray(int size) {
            return new J_reqModelGift[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(model_name);
    }
}
