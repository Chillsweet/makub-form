package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqFormRequestList implements Parcelable {

    private String last_update;

    public J_reqFormRequestList(String last_update) {
        this.last_update = last_update;
    }

    protected J_reqFormRequestList(Parcel in) {
        last_update = in.readString();
    }

    public static final Creator<J_reqFormRequestList> CREATOR = new Creator<J_reqFormRequestList>() {
        @Override
        public J_reqFormRequestList createFromParcel(Parcel in) {
            return new J_reqFormRequestList(in);
        }

        @Override
        public J_reqFormRequestList[] newArray(int size) {
            return new J_reqFormRequestList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(last_update);
    }
}
