package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_approverList implements Parcelable{

    private String full_name; //(string, optional),
    private String leave_state_id; //(integer, optional),
    private String leave_state_name; //(string, optional),
    private String approver_reason; //(string, optional)

    public String getFull_name() {
        return full_name;
    }

    public String getLeave_state_id() {
        return leave_state_id;
    }

    public String getLeave_state_name() {
        return leave_state_name;
    }

    public String getApprover_reason() {
        return approver_reason;
    }

    protected M_approverList(Parcel in) {
        full_name = in.readString();
        leave_state_id = in.readString();
        leave_state_name = in.readString();
        approver_reason = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(full_name);
        dest.writeString(leave_state_id);
        dest.writeString(leave_state_name);
        dest.writeString(approver_reason);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_approverList> CREATOR = new Creator<M_approverList>() {
        @Override
        public M_approverList createFromParcel(Parcel in) {
            return new M_approverList(in);
        }

        @Override
        public M_approverList[] newArray(int size) {
            return new M_approverList[size];
        }
    };
}
