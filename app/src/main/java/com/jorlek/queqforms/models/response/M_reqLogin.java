package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_reqLogin implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private String token; //(string, optional): token ,
    private String key_aes; //(string, optional): key เข้ารหัส ,
    private boolean reset_flag; //(boolean, optional): ถ้าเป็น true = ต้องเปลี่ยน pin ,
    private String current_lang; //(string, optional): ภาษาเครื่องปัจจุบัณ ,
    private boolean lock_flag; //(boolean, optional): true = lock

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public String getToken() {
        return token;
    }

    public String getKey_aes() {
        return key_aes;
    }

    public boolean isReset_flag() {
        return reset_flag;
    }

    public String getCurrent_lang() {
        return current_lang;
    }

    public boolean isLock_flag() {
        return lock_flag;
    }

    protected M_reqLogin(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        token = in.readString();
        key_aes = in.readString();
        reset_flag = in.readByte() != 0;
        current_lang = in.readString();
        lock_flag = in.readByte() != 0;
    }

    public static final Creator<M_reqLogin> CREATOR = new Creator<M_reqLogin>() {
        @Override
        public M_reqLogin createFromParcel(Parcel in) {
            return new M_reqLogin(in);
        }

        @Override
        public M_reqLogin[] newArray(int size) {
            return new M_reqLogin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeString(token);
        parcel.writeString(key_aes);
        parcel.writeByte((byte) (reset_flag ? 1 : 0));
        parcel.writeString(current_lang);
        parcel.writeByte((byte) (lock_flag ? 1 : 0));
    }
}
