package com.jorlek.queqforms.models.request;

/**
 * Created by ChillSweet on 4/20/2016.
 */
public class J_Form_Question {

    public String form_question_id;
    public String name;
    public String val;

    public String getForm_question_id() {
        return form_question_id;
    }

    public void setForm_question_id(String form_question_id) {
        this.form_question_id = form_question_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

}
