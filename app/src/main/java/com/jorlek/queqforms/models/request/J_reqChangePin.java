package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqChangePin implements Parcelable {

    private String old_pin; //(string): old_password ,
    private String new_pin; //(string): new_password

    public J_reqChangePin(String old_pin, String new_pin) {
        this.old_pin = old_pin;
        this.new_pin = new_pin;
    }

    protected J_reqChangePin(Parcel in) {
        old_pin = in.readString();
        new_pin = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(old_pin);
        dest.writeString(new_pin);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<J_reqChangePin> CREATOR = new Creator<J_reqChangePin>() {
        @Override
        public J_reqChangePin createFromParcel(Parcel in) {
            return new J_reqChangePin(in);
        }

        @Override
        public J_reqChangePin[] newArray(int size) {
            return new J_reqChangePin[size];
        }
    };
}
