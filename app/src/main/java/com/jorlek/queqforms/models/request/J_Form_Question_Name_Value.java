package com.jorlek.queqforms.models.request;

/**
 * Created by ChillSweet on 4/20/2016.
 */
public class J_Form_Question_Name_Value {

    private String name;
    private String val;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }


}
