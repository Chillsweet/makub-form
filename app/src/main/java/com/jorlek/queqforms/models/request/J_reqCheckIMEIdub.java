package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqCheckIMEIdub implements Parcelable {

    private String imei_no; //(string): IMEI Customer

    public J_reqCheckIMEIdub(String imei_no) {
        this.imei_no = imei_no;
    }

    protected J_reqCheckIMEIdub(Parcel in) {
        imei_no = in.readString();
    }

    public static final Creator<J_reqCheckIMEIdub> CREATOR = new Creator<J_reqCheckIMEIdub>() {
        @Override
        public J_reqCheckIMEIdub createFromParcel(Parcel in) {
            return new J_reqCheckIMEIdub(in);
        }

        @Override
        public J_reqCheckIMEIdub[] newArray(int size) {
            return new J_reqCheckIMEIdub[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imei_no);
    }
}
