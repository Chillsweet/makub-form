package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChillSweet on 4/20/2016.
 */
public class J_sendMailReportFormList implements Parcelable {

    private String form_datetime; //(string, optional),
    private String to_datetime; //(string, optional),
    private int report_type; //(integer, optional),
    private String report_text; //(string, optional)

    public J_sendMailReportFormList(String form_datetime, String to_datetime, int report_type, String report_text) {
        this.form_datetime = form_datetime;
        this.to_datetime = to_datetime;
        this.report_type = report_type;
        this.report_text = report_text;
    }

    protected J_sendMailReportFormList(Parcel in) {
        form_datetime = in.readString();
        to_datetime = in.readString();
        report_type = in.readInt();
        report_text = in.readString();
    }

    public static final Creator<J_sendMailReportFormList> CREATOR = new Creator<J_sendMailReportFormList>() {
        @Override
        public J_sendMailReportFormList createFromParcel(Parcel in) {
            return new J_sendMailReportFormList(in);
        }

        @Override
        public J_sendMailReportFormList[] newArray(int size) {
            return new J_sendMailReportFormList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(form_datetime);
        parcel.writeString(to_datetime);
        parcel.writeInt(report_type);
        parcel.writeString(report_text);
    }
}
