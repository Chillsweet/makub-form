package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/20/2017 AD.
 */

public class M_fileData implements Parcelable {

    private int file_size; //(integer, optional): file_size ,
    private String file_name; //(string, optional): file_name ,
    private String full_path; //(string, optional): for use show ,
    private String save_path; //(string, optional): for send to save

    public int getFile_size() {
        return file_size;
    }

    public String getFile_name() {
        return file_name;
    }

    public String getFull_path() {
        return full_path;
    }

    public String getSave_path() {
        return save_path;
    }

    protected M_fileData(Parcel in) {
        file_size = in.readInt();
        file_name = in.readString();
        full_path = in.readString();
        save_path = in.readString();
    }

    public static final Creator<M_fileData> CREATOR = new Creator<M_fileData>() {
        @Override
        public M_fileData createFromParcel(Parcel in) {
            return new M_fileData(in);
        }

        @Override
        public M_fileData[] newArray(int size) {
            return new M_fileData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(file_size);
        parcel.writeString(file_name);
        parcel.writeString(full_path);
        parcel.writeString(save_path);
    }
}
