package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqCreateFormByEmp implements Parcelable {

    private String form_id;
    private String emp_form_value;

    public J_reqCreateFormByEmp(String form_id, String emp_form_value) {
        this.form_id = form_id;
        this.emp_form_value = emp_form_value;
    }

    protected J_reqCreateFormByEmp(Parcel in) {
        form_id = in.readString();
        emp_form_value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(form_id);
        dest.writeString(emp_form_value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<J_reqCreateFormByEmp> CREATOR = new Creator<J_reqCreateFormByEmp>() {
        @Override
        public J_reqCreateFormByEmp createFromParcel(Parcel in) {
            return new J_reqCreateFormByEmp(in);
        }

        @Override
        public J_reqCreateFormByEmp[] newArray(int size) {
            return new J_reqCreateFormByEmp[size];
        }
    };
}
