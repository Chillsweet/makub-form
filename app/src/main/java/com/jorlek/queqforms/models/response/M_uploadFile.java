package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_uploadFile implements Parcelable{

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private M_fileData file_data; //(FileMo, optional): list file

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public M_fileData getFile_data() {
        return file_data;
    }

    protected M_uploadFile(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        file_data = in.readParcelable(M_fileData.class.getClassLoader());
    }

    public static final Creator<M_uploadFile> CREATOR = new Creator<M_uploadFile>() {
        @Override
        public M_uploadFile createFromParcel(Parcel in) {
            return new M_uploadFile(in);
        }

        @Override
        public M_uploadFile[] newArray(int size) {
            return new M_uploadFile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeParcelable(file_data, i);
    }
}
