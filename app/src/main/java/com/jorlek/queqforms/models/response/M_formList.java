package com.jorlek.queqforms.models.response;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_formList implements Parcelable {

    private String emp_form_id; //(string, optional),
    private String emp_fullname; //(string, optional),
    private String emp_picture; //(string, optional),
    private String emp_position; //(string, optional),
    private String form_id; //(string, optional),
    private String form_title; //(string, optional),
    private String form_desc; //(string, optional),
    private String form_detail; //(string, optional),
    private String created_datetime; //(string, optional),
    private String emp_form_value; //(string, optional): http://uat.makub.me/files/other/json_text.png ,
    private int form_state_id; //(integer, optional),
    private String form_state_name; //(string, optional),
    private ArrayList<M_formQuestionList> form_question_list = new ArrayList<>(); //(Array[formQuestionList], optional),
    private ArrayList<M_approverList> approver_list = new ArrayList<>(); //(Array[ApproverData], optional)

    public String getEmp_form_id() {
        return emp_form_id;
    }

    public String getEmp_fullname() {
        return emp_fullname;
    }

    public String getEmp_picture() {
        return emp_picture;
    }

    public String getEmp_position() {
        return emp_position;
    }

    public String getForm_id() {
        return form_id;
    }

    public String getForm_title() {
        return form_title;
    }

    public String getForm_desc() {
        return form_desc;
    }

    public String getForm_detail() {
        return form_detail;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public String getEmp_form_value() {
        return emp_form_value;
    }

    public int getForm_state_id() {
        return form_state_id;
    }

    public String getForm_state_name() {
        return form_state_name;
    }

    public ArrayList<M_formQuestionList> getForm_question_list() {
        return form_question_list;
    }

    public ArrayList<M_approverList> getApprover_list() {
        return approver_list;
    }

    protected M_formList(Parcel in) {
        emp_form_id = in.readString();
        emp_fullname = in.readString();
        emp_picture = in.readString();
        emp_position = in.readString();
        form_id = in.readString();
        form_title = in.readString();
        form_desc = in.readString();
        form_detail = in.readString();
        created_datetime = in.readString();
        emp_form_value = in.readString();
        form_state_id = in.readInt();
        form_state_name = in.readString();
        form_question_list = in.createTypedArrayList(M_formQuestionList.CREATOR);
        approver_list = in.createTypedArrayList(M_approverList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emp_form_id);
        dest.writeString(emp_fullname);
        dest.writeString(emp_picture);
        dest.writeString(emp_position);
        dest.writeString(form_id);
        dest.writeString(form_title);
        dest.writeString(form_desc);
        dest.writeString(form_detail);
        dest.writeString(created_datetime);
        dest.writeString(emp_form_value);
        dest.writeInt(form_state_id);
        dest.writeString(form_state_name);
        dest.writeTypedList(form_question_list);
        dest.writeTypedList(approver_list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_formList> CREATOR = new Creator<M_formList>() {
        @Override
        public M_formList createFromParcel(Parcel in) {
            return new M_formList(in);
        }

        @Override
        public M_formList[] newArray(int size) {
            return new M_formList[size];
        }
    };
}
