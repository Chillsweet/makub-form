package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class J_reqFormHistoryList implements Parcelable {

    private int page_index; //(integer, optional): page number ,
    private int page_size; //(integer, optional): page size

    public void setPage_index(int page_index) {
        this.page_index = page_index;
    }

    public void setPage_size(int page_size) {
        this.page_size = page_size;
    }

    public J_reqFormHistoryList(int page_index, int page_size) {
        this.page_index = page_index;
        this.page_size = page_size;
    }

    protected J_reqFormHistoryList(Parcel in) {
        page_index = in.readInt();
        page_size = in.readInt();
    }

    public static final Creator<J_reqFormHistoryList> CREATOR = new Creator<J_reqFormHistoryList>() {
        @Override
        public J_reqFormHistoryList createFromParcel(Parcel in) {
            return new J_reqFormHistoryList(in);
        }

        @Override
        public J_reqFormHistoryList[] newArray(int size) {
            return new J_reqFormHistoryList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(page_index);
        parcel.writeInt(page_size);
    }
}
