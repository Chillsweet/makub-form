package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_reqCheckDub implements Parcelable{

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private boolean dub_flag; //(boolean, optional): dub = true ,
    private String title;
    private ArrayList<M_formQuestionList> form_question_list = new ArrayList<>(); //(Array[FormDubData], optional)

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public boolean isDub_flag() {
        return dub_flag;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<M_formQuestionList> getForm_question_list() {
        return form_question_list;
    }

    protected M_reqCheckDub(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        dub_flag = in.readByte() != 0;
        title = in.readString();
        form_question_list = in.createTypedArrayList(M_formQuestionList.CREATOR);
    }

    public static final Creator<M_reqCheckDub> CREATOR = new Creator<M_reqCheckDub>() {
        @Override
        public M_reqCheckDub createFromParcel(Parcel in) {
            return new M_reqCheckDub(in);
        }

        @Override
        public M_reqCheckDub[] newArray(int size) {
            return new M_reqCheckDub[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeByte((byte) (dub_flag ? 1 : 0));
        parcel.writeString(title);
        parcel.writeTypedList(form_question_list);
    }
}
