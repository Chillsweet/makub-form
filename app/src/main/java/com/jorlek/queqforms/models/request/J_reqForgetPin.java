package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChillSweet on 4/20/2016.
 */
public class J_reqForgetPin implements Parcelable {

    private String company_code; //(string, optional): company_code ,
    private String user_name; //(string, optional): user_name ,
    private String device_code; //(string, optional): device_code

    public J_reqForgetPin(String company_code, String user_name, String device_code) {
        this.company_code = company_code;
        this.user_name = user_name;
        this.device_code = device_code;
    }

    protected J_reqForgetPin(Parcel in) {
        company_code = in.readString();
        user_name = in.readString();
        device_code = in.readString();
    }

    public static final Creator<J_reqForgetPin> CREATOR = new Creator<J_reqForgetPin>() {
        @Override
        public J_reqForgetPin createFromParcel(Parcel in) {
            return new J_reqForgetPin(in);
        }

        @Override
        public J_reqForgetPin[] newArray(int size) {
            return new J_reqForgetPin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(company_code);
        parcel.writeString(user_name);
        parcel.writeString(device_code);
    }
}
