package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/18/2017 AD.
 */

public class M_reqForgetPin implements Parcelable{

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private String message; //(string, optional): emp_id

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public String getMessage() {
        return message;
    }

    protected M_reqForgetPin(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        message = in.readString();
    }

    public static final Creator<M_reqForgetPin> CREATOR = new Creator<M_reqForgetPin>() {
        @Override
        public M_reqForgetPin createFromParcel(Parcel in) {
            return new M_reqForgetPin(in);
        }

        @Override
        public M_reqForgetPin[] newArray(int size) {
            return new M_reqForgetPin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
        parcel.writeString(message);
    }
}
