package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqLogin implements Parcelable {

    private String device_id; //(string): device_id ,
    private String device_token; //(string, optional): device_token for notification ,
    private int device_type; //(integer): 1=Android 2=IPhone 3=IPad ,
    private String device_os_version; //(string, optional): device_os_version ,
    private String app_version; //(string, optional): makub now version ,
    private String device_model; //(string, optional): device model ex:6s+,galaxy s7

    public J_reqLogin(String device_id, String device_token, int device_type, String device_os_version, String app_version, String device_model) {
        this.device_id = device_id;
        this.device_token = device_token;
        this.device_type = device_type;
        this.device_os_version = device_os_version;
        this.app_version = app_version;
        this.device_model = device_model;
    }

    protected J_reqLogin(Parcel in) {
        device_id = in.readString();
        device_token = in.readString();
        device_type = in.readInt();
        device_os_version = in.readString();
        app_version = in.readString();
        device_model = in.readString();
    }

    public static final Creator<J_reqLogin> CREATOR = new Creator<J_reqLogin>() {
        @Override
        public J_reqLogin createFromParcel(Parcel in) {
            return new J_reqLogin(in);
        }

        @Override
        public J_reqLogin[] newArray(int size) {
            return new J_reqLogin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(device_id);
        parcel.writeString(device_token);
        parcel.writeInt(device_type);
        parcel.writeString(device_os_version);
        parcel.writeString(app_version);
        parcel.writeString(device_model);
    }
}
