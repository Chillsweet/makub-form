package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ChillSweet on 4/20/2016.
 */
public class M_sendMailReportFormList implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional)

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    protected M_sendMailReportFormList(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
    }

    public static final Creator<M_sendMailReportFormList> CREATOR = new Creator<M_sendMailReportFormList>() {
        @Override
        public M_sendMailReportFormList createFromParcel(Parcel in) {
            return new M_sendMailReportFormList(in);
        }

        @Override
        public M_sendMailReportFormList[] newArray(int size) {
            return new M_sendMailReportFormList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(result_code);
        parcel.writeString(result_desc);
    }
}
