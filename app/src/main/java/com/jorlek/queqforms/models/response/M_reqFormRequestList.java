package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_reqFormRequestList implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private String last_update; //(string, optional),
    private ArrayList<M_formRequestList> form_list = new ArrayList<>(); //(Array[formRequestList], optional),
    private boolean write_flag; //(boolean, optional)

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public String getLast_update() {
        return last_update;
    }

    public ArrayList<M_formRequestList> getForm_list() {
        return form_list;
    }

    public boolean isWrite_flag() {
        return write_flag;
    }

    protected M_reqFormRequestList(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        last_update = in.readString();
        form_list = in.createTypedArrayList(M_formRequestList.CREATOR);
        write_flag = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result_code);
        dest.writeString(result_desc);
        dest.writeString(last_update);
        dest.writeTypedList(form_list);
        dest.writeByte((byte) (write_flag ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_reqFormRequestList> CREATOR = new Creator<M_reqFormRequestList>() {
        @Override
        public M_reqFormRequestList createFromParcel(Parcel in) {
            return new M_reqFormRequestList(in);
        }

        @Override
        public M_reqFormRequestList[] newArray(int size) {
            return new M_reqFormRequestList[size];
        }
    };
}
