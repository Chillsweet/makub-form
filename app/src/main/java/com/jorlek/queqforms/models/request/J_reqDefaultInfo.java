package com.jorlek.queqforms.models.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class J_reqDefaultInfo implements Parcelable {

    private String device_code; //(string): รหัสเครื่อง ,
    private String device_token; //(string, optional): device_token ,
    private String device_language; //(string, optional): ภาษาเครื่อง (th|en)

    public J_reqDefaultInfo(String device_code, String device_token, String device_language) {
        this.device_code = device_code;
        this.device_token = device_token;
        this.device_language = device_language;
    }

    public J_reqDefaultInfo(Parcel in) {
        device_code = in.readString();
        device_token = in.readString();
        device_language = in.readString();
    }

    public static final Creator<J_reqDefaultInfo> CREATOR = new Creator<J_reqDefaultInfo>() {
        @Override
        public J_reqDefaultInfo createFromParcel(Parcel in) {
            return new J_reqDefaultInfo(in);
        }

        @Override
        public J_reqDefaultInfo[] newArray(int size) {
            return new J_reqDefaultInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(device_code);
        parcel.writeString(device_token);
        parcel.writeString(device_language);
    }
}
