package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_reqChangePin implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    protected M_reqChangePin(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result_code);
        dest.writeString(result_desc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_reqChangePin> CREATOR = new Creator<M_reqChangePin>() {
        @Override
        public M_reqChangePin createFromParcel(Parcel in) {
            return new M_reqChangePin(in);
        }

        @Override
        public M_reqChangePin[] newArray(int size) {
            return new M_reqChangePin[size];
        }
    };
}
