package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_reqCheckLock implements Parcelable {

    private String result_code; //(string, optional),
    private String result_desc; //(string, optional),
    private boolean lock_flag; //(boolean, optional): ถ้าเป็น true คือถูก lock ,
    private String lock_message; //(string, optional): ข้อความที่แสดงเวลาเครื่องถูกล๊อก ,
    private int lock_second; //(integer, optional): เวลานับถอยหลังเป็นวินาที

    public String getResult_code() {
        return result_code;
    }

    public String getResult_desc() {
        return result_desc;
    }

    public boolean isLock_flag() {
        return lock_flag;
    }

    public String getLock_message() {
        return lock_message;
    }

    public int getLock_second() {
        return lock_second;
    }

    protected M_reqCheckLock(Parcel in) {
        result_code = in.readString();
        result_desc = in.readString();
        lock_flag = in.readByte() != 0;
        lock_message = in.readString();
        lock_second = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result_code);
        dest.writeString(result_desc);
        dest.writeByte((byte) (lock_flag ? 1 : 0));
        dest.writeString(lock_message);
        dest.writeInt(lock_second);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_reqCheckLock> CREATOR = new Creator<M_reqCheckLock>() {
        @Override
        public M_reqCheckLock createFromParcel(Parcel in) {
            return new M_reqCheckLock(in);
        }

        @Override
        public M_reqCheckLock[] newArray(int size) {
            return new M_reqCheckLock[size];
        }
    };
}
