package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_formRequestList implements Parcelable {

    private String form_id; //(string, optional),
    private String form_master_id; //(string, optional),
    private String form_icon; //(string, optional),
    private String form_title; //(string, optional),
    private String form_desc; //(string, optional),
    private String form_detail; //(string, optional),
    private String form_remark; //(string, optional),
    private ArrayList<M_formQuestionList> form_question = new ArrayList<>(); //(Array[formQuestionList], optional)

    public String getForm_id() {
        return form_id;
    }

    public String getForm_master_id() {
        return form_master_id;
    }

    public String getForm_icon() {
        return form_icon;
    }

    public String getForm_title() {
        return form_title;
    }

    public String getForm_desc() {
        return form_desc;
    }

    public String getForm_detail() {
        return form_detail;
    }

    public String getForm_remark() {
        return form_remark;
    }

    public ArrayList<M_formQuestionList> getForm_question() {
        return form_question;
    }

    protected M_formRequestList(Parcel in) {
        form_id = in.readString();
        form_master_id = in.readString();
        form_icon = in.readString();
        form_title = in.readString();
        form_desc = in.readString();
        form_detail = in.readString();
        form_remark = in.readString();
        form_question = in.createTypedArrayList(M_formQuestionList.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(form_id);
        dest.writeString(form_master_id);
        dest.writeString(form_icon);
        dest.writeString(form_title);
        dest.writeString(form_desc);
        dest.writeString(form_detail);
        dest.writeString(form_remark);
        dest.writeTypedList(form_question);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<M_formRequestList> CREATOR = new Creator<M_formRequestList>() {
        @Override
        public M_formRequestList createFromParcel(Parcel in) {
            return new M_formRequestList(in);
        }

        @Override
        public M_formRequestList[] newArray(int size) {
            return new M_formRequestList[size];
        }
    };
}
