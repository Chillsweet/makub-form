package com.jorlek.queqforms.models.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class M_formQuestionList implements Parcelable {

    private String form_question_id; //(string, optional),
    private String form_question_title; //(string, optional),
    private String form_question_placeholder; //(string, optional),
    private String form_question_default; //(string, optional),
    private String form_question_default_nd; //(string, optional),
    private boolean form_question_require_field; //(boolean, optional): true = require and false = not require ,
    private int input_type_id; //(integer, optional),
    private String input_type_name; //(string, optional),
    private String input_type_regular; //(string, optional)

    public String getForm_question_id() {
        return form_question_id;
    }

    public String getForm_question_title() {
        return form_question_title;
    }

    public String getForm_question_placeholder() {
        return form_question_placeholder;
    }

    public String getForm_question_default() {
        return form_question_default;
    }

    public String getForm_question_default_nd() {
        return form_question_default_nd;
    }

    public boolean isForm_question_require_field() {
        return form_question_require_field;
    }

    public int getInput_type_id() {
        return input_type_id;
    }

    public String getInput_type_name() {
        return input_type_name;
    }

    public String getInput_type_regular() {
        return input_type_regular;
    }

    protected M_formQuestionList(Parcel in) {
        form_question_id = in.readString();
        form_question_title = in.readString();
        form_question_placeholder = in.readString();
        form_question_default = in.readString();
        form_question_default_nd = in.readString();
        form_question_require_field = in.readByte() != 0;
        input_type_id = in.readInt();
        input_type_name = in.readString();
        input_type_regular = in.readString();
    }

    public static final Creator<M_formQuestionList> CREATOR = new Creator<M_formQuestionList>() {
        @Override
        public M_formQuestionList createFromParcel(Parcel in) {
            return new M_formQuestionList(in);
        }

        @Override
        public M_formQuestionList[] newArray(int size) {
            return new M_formQuestionList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(form_question_id);
        parcel.writeString(form_question_title);
        parcel.writeString(form_question_placeholder);
        parcel.writeString(form_question_default);
        parcel.writeString(form_question_default_nd);
        parcel.writeByte((byte) (form_question_require_field ? 1 : 0));
        parcel.writeInt(input_type_id);
        parcel.writeString(input_type_name);
        parcel.writeString(input_type_regular);
    }
}
