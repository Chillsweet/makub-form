package com.jorlek.queqforms;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jorlek.queqforms.custom.DialogCreate;
import com.jorlek.queqforms.helper.ExifUtils;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.login.LoginActivity;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by chillsweet on 4/4/2017 AD.
 */

public class Global {

    public static String DEVICE_ID;
    public static String DEVICE_BRAND;
    public static String DEVICE_MODEL;
    public static String DEVICE_OS;
    public static String VERSION_APP;

    public static DialogInterface.OnClickListener getErrorDialogOnClick(Activity activity, String dialogType) {

        DialogInterface.OnClickListener listener = null;

        if (dialogType.equals("login")) {
            listener = (dialogInterface, i) -> {
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.finish();
                activity.overridePendingTransition(0, 0);
                activity.startActivity(intent);
            };
        }
        else {
            listener = (dialogInterface, i) -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    activity.finishAffinity();
                }
                else {
                    ActivityCompat.finishAffinity(activity);
                }
            };
        }

        return listener;
    }

    public static void showTokenExpireDialog(Activity activity) {
        new DialogCreate(activity).AlertError(activity.getString(R.string.check_expire_message),
                activity.getString(R.string.check_expire_button), getErrorDialogOnClick(activity, "login"));
    }

    public static void showTimeOutDialog(Activity activity) {
        new DialogCreate(activity).AlertError(activity.getString(R.string.service_timeout),
                activity.getString(R.string.check_expire_button), getErrorDialogOnClick(activity, "login"));
    }

    public static void showToast(Activity activity, String msg) {
        if (BuildConfig.DEBUG) {
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
        }
    }

    public static void hideSoftKeyboard(Context context, EditText input) {
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    public static void getAboutDevice(Context context) {

        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Global.DEVICE_ID = android.provider.Settings.System.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        Global.DEVICE_BRAND = Build.MODEL;
        Global.DEVICE_MODEL = Build.MANUFACTURER;
        Global.DEVICE_OS = Build.VERSION.RELEASE;
        Global.VERSION_APP = pInfo.versionName;
    }

    public static boolean isStringEmpty(String input) {
        if (input != null && !input.equals(""))
            return false;
        return true;
    }

    public static boolean isNetworkOnline(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected())
            return true;
        else {
            new DialogCreate(context).AlertForNetWork(context.getString(R.string.connection_lost));
            return false;
        }

    }

    public static SpannableString setUnderLineText(String text) {
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    public static String buildDateFormat(int date, int month, int year, String lang) {
        String result = "";

        if (date < 10) result = result + "0" + date;
        else result = result + date;

        if (month + 1 < 10) result = result + "/" + "0" + (month + 1);
        else result = result + "/" + (month + 1);

        if (lang.equals("th")) result = result + "/" + (year + 543);
        else result = result + "/" + year;

        return result;
    }

    public static String buildTimeFormat(int hrs, int min) {
        String result, hrsStr, minStr;

        if (hrs >= 0 && hrs <= 9) hrsStr = "0" + String.valueOf(hrs);
        else hrsStr = String.valueOf(hrs);

        if (min >= 0 && min <= 9) minStr = "0" + String.valueOf(min);
        else minStr = String.valueOf(min);

        result = hrsStr + ":" + minStr;

        return result;
    }

    public static <T> T convertGsonModel(String input) {
        String json = "";
        try {
            json = URLDecoder.decode(input, "UTF-8");
            writeLog.LogE("convertGsonModel", json);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        String jsonOutput = json;
        Type listType = new TypeToken<List<J_Form_Question_Name_Value>>() {
        }.getType();
        return gson.fromJson(jsonOutput, listType);
    }

    public static void saveToInternalStorage(Context context, Bitmap imageBitmap, String fileName) {

        File file, f = null;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            file = new File("/data/data/" + context.getPackageName());
            if (!file.exists()) {
                file.mkdirs();
            }
            f = new File(file.getAbsolutePath() + file.separator + fileName);
        }
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 90, ostream);
        try {
            ostream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void setImageFromInternalStorage(Context mContext, ImageView imageView, String fileName) {
        File file_profile = new File("/data/data/" + mContext.getPackageName() + "/" + fileName);
        try {
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file_profile));
            imageView.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getBitmapResize(Context context, String path) {
        ContentResolver mContentResolver = context.getContentResolver();
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        InputStream in = null;
        Bitmap bitmap;
        try {
            in = mContentResolver.openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
            in = null;
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();
            if (bitmap.getHeight() > bitmap.getWidth()) {
                if (bitmap.getHeight() > 1920) {
                    height = 1920;
                    width = Math.round(height * aspectRatio);
                    System.gc();
                    return Bitmap.createScaledBitmap(bitmap, width, height, true);
                }
                else {
                    System.gc();
                    return bitmap;
                }
            }
            else {
                if (bitmap.getWidth() > 1920) {
                    width = 1920;
                    height = Math.round(width / aspectRatio);
                    System.gc();
                    return Bitmap.createScaledBitmap(bitmap, width, height, true);
                }
                else {
                    System.gc();
                    return bitmap;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File createImageFile(Context context, String name) {
        File mFile = new File(context.getFilesDir(), name);

        if (!mFile.exists()) {
            mFile.delete();
            try {
                mFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mFile;
    }

    public static void writeImageFile(Bitmap image, String pathFolder) {
        try {
            FileOutputStream stream = new FileOutputStream(pathFolder);
            image.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            stream.flush();
            stream.close();
            image.recycle();
            stream = null;
            image = null;
            System.gc();

        } catch (Exception e) {
        }
    }

    public static Bitmap decodeFile(String filePath) {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap b1 = BitmapFactory.decodeFile(filePath, o2);
        Bitmap b = ExifUtils.rotateBitmap(filePath, b1);
        return b;

        // image.setImageBitmap(bitmap);
    }

}
