package com.jorlek.queqforms.helper;

import android.util.Log;

import com.jorlek.queqforms.AppConfig;
import com.jorlek.queqforms.BuildConfig;

public class writeLog {

	public static boolean DEBUG = AppConfig.DEBUG;

	public static void LogI(String TAG, String message) {
		if (!BuildConfig.DEBUG) {
			DEBUG = false;
		}
		if (DEBUG)
			Log.i(TAG, message);
	}

	public static void LogE(String TAG, String message) {
		if (!BuildConfig.DEBUG) {
			DEBUG = false;
		}
		if (DEBUG)
			Log.e(TAG, message);
	}

	public static void LogD(String TAG, String message) {
		if (!BuildConfig.DEBUG) {
			DEBUG = false;
		}
		if (DEBUG)
			Log.d(TAG, message);
	}
}