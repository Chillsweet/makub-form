package com.jorlek.queqforms.services;

import com.jorlek.queqforms.models.request.J_reqChangePin;
import com.jorlek.queqforms.models.request.J_reqCheckCarddub;
import com.jorlek.queqforms.models.request.J_reqCheckIMEIdub;
import com.jorlek.queqforms.models.request.J_reqCreateFormByEmp;
import com.jorlek.queqforms.models.request.J_reqDefaultInfo;
import com.jorlek.queqforms.models.request.J_reqForgetPin;
import com.jorlek.queqforms.models.request.J_reqFormHistoryList;
import com.jorlek.queqforms.models.request.J_reqFormRequestList;
import com.jorlek.queqforms.models.request.J_reqLogin;
import com.jorlek.queqforms.models.request.J_reqModelGift;
import com.jorlek.queqforms.models.request.J_sendMailReportFormList;
import com.jorlek.queqforms.models.response.M_reqChangePin;
import com.jorlek.queqforms.models.response.M_reqCheckDub;
import com.jorlek.queqforms.models.response.M_reqCheckLock;
import com.jorlek.queqforms.models.response.M_reqCreateFormByEmp;
import com.jorlek.queqforms.models.response.M_reqDefaultInfo;
import com.jorlek.queqforms.models.response.M_reqForgetPin;
import com.jorlek.queqforms.models.response.M_reqFormHistoryList;
import com.jorlek.queqforms.models.response.M_reqFormRequestList;
import com.jorlek.queqforms.models.response.M_reqLogin;
import com.jorlek.queqforms.models.response.M_reqModelGift;
import com.jorlek.queqforms.models.response.M_sendMailReportFormList;
import com.jorlek.queqforms.models.response.M_uploadFile;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public interface CallApi {

    /**
     * Registration
     */
    @POST("Authenticate/reqDefaultInfo")
    Observable<M_reqDefaultInfo> reqDefaultInfo(@Header("X-QueQForms-AuthCompanyCode") String AuthCompanyCode,
                                                @Header("X-QueQForms-AuthUsername") String AuthUsername,
                                                @Body J_reqDefaultInfo jReqDefaultInfo);

    /**
     * Login
     */
    @POST("Authenticate/reqLogin")
    Observable<M_reqLogin> reqLogin(@Header("X-QueQForms-AuthCompanyCode") String AuthCompanyCode,
                                    @Header("X-QueQForms-AuthUsername") String AuthUsername,
                                    @Header("X-QueQForms-AuthPassword") String AuthPassword,
                                    @Body J_reqLogin jReqLogin);

    @POST("Authenticate/reqCheckLock")
    Observable<M_reqCheckLock> reqCheckLock(@Header("X-QueQForms-AuthDeviceCode") String AuthDeviceCode);

    @POST("Authenticate/reqChangePin")
    Observable<M_reqChangePin> reqChangePin(@Header("X-QueQForms-AuthToken") String AuthToken,
                                            @Body J_reqChangePin jReqChangePin);

    @POST("Authenticate/reqForgetPin")
    Observable<M_reqForgetPin> reqForgetPin(@Header("X-QueQForms-AuthToken") String AuthToken,
                                            @Body J_reqForgetPin jReqForgetPin);


    /**
     * Main
     */
    @POST("RequestForm/reqFormRequestList")
    Observable<M_reqFormRequestList> reqFormRequestList(@Header("X-QueQForms-AuthToken") String AuthToken,
                                                        @Body J_reqFormRequestList jReqFormRequestList);


    @POST("RequestForm/reqCheckIMEIdub")
    Observable<M_reqCheckDub> reqCheckIMEIdub(@Header("X-QueQForms-AuthToken") String AuthToken,
                                              @Body J_reqCheckIMEIdub jReqCheckIMEIdub);

    @POST("RequestForm/reqModelGift")
    Observable<M_reqModelGift> reqModelGift(@Header("X-QueQForms-AuthToken") String AuthToken,
                                            @Body J_reqModelGift jReqModelGift);

    @POST("RequestForm/reqCheckIdCarddub")
    Observable<M_reqCheckDub> reqCheckIdCarddub(@Header("X-QueQForms-AuthToken") String AuthToken,
                                                @Body J_reqCheckCarddub jReqCheckCarddub);

    @POST("RequestForm/reqCreateFormByEmp")
    Observable<M_reqCreateFormByEmp> reqCreateFormByEmp(@Header("X-QueQForms-AuthToken") String AuthToken,
                                                        @Body J_reqCreateFormByEmp jReqCreateFormByEmp);

    @Multipart
    @POST("Upload/File")
    Observable<M_uploadFile> uploadFile(@Header("X-QueQForms-AuthToken") String AuthToken,
                                        @Part MultipartBody.Part image,
                                        @Part("name") RequestBody name);

    /**
     * History
     */
    @POST("RequestForm/reqFormHistoryList")
    Observable<M_reqFormHistoryList> reqFormHistoryList(@Header("X-QueQForms-AuthToken") String AuthToken,
                                                        @Body J_reqFormHistoryList jReqFormHistoryList);

    /**
     * Report
     */
    @POST("RequestForm/sendMailReportFormList")
    Observable<M_sendMailReportFormList> sendMailReportFormList(@Header("X-QueQForms-AuthToken") String AuthToken,
                                                                @Body J_sendMailReportFormList jSendMailReportFormList);

}
