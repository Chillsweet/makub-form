package com.jorlek.queqforms.services;

/**
 * Created by chillsweet on 11/23/2016 AD.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
