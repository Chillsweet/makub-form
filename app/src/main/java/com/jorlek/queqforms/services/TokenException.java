package com.jorlek.queqforms.services;

@SuppressWarnings("serial")
public class TokenException extends Exception {
	public TokenException(String exc) {
		super(exc);
	}

	public String getMessage() {
		return super.getMessage();
	}
}