package com.jorlek.queqforms.services;

import android.annotation.SuppressLint;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEngine {
    private static Cipher _Cipher;
    //	private String key = "42f7339cebb14fe7931d1f78711669d7";
    final static String salt = "dXfCrM5vZqDNOd5yiOqtaRtAe68zEUhN";
    private SecretKeySpec _SKeySpec;
    final int ITERATIONS = 10000;
    final int BLOCK_SIZE = 128;
    final int KEY_SIZE = 256;
    final int IV_BYTE_SIZE = BLOCK_SIZE / 8;
    // final int FILE_BLOCK_BYTE_SIZE = 64 * (BLOCK_SIZE / 8);
    final int FILE_BLOCK_BYTE_SIZE = 64 * 64 * 64 * (BLOCK_SIZE / 8);

    public AESEngine(String key) throws Exception {
        char[] keyChars = new char[key.length()];
        key.getChars(0, key.length(), keyChars, 0);
        SecretKeyFactory factory = SecretKeyFactory
                .getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(keyChars, salt.getBytes(), ITERATIONS,
                KEY_SIZE);
        SecretKey sKey = factory.generateSecret(spec);
        byte[] raw = sKey.getEncoded();
        _SKeySpec = new SecretKeySpec(raw, "AES");
        _Cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    }

    public String encrypt(String text) throws Exception {
        byte[] inputBytes = text.getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBytes);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        encrypt(inputStream, outputStream);
        byte[] resultBytes = outputStream.toByteArray();
        inputStream.close();
        outputStream.close();
        return Base64.encodeToString(resultBytes, Base64.NO_WRAP);
    }

    public void encrypt(InputStream inputStream, OutputStream outputStream)
            throws Exception {
        byte[] ivBytes = generateIV();
        outputStream.write(ivBytes, 0, ivBytes.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
        _Cipher.init(Cipher.ENCRYPT_MODE, _SKeySpec, ivParameterSpec);
        CipherOutputStream cryptoStream = new CipherOutputStream(outputStream,
                _Cipher);
        int readSize;
        byte[] inputBytes = new byte[FILE_BLOCK_BYTE_SIZE];
        while ((readSize = inputStream
                .read(inputBytes, 0, FILE_BLOCK_BYTE_SIZE)) > 0)
            cryptoStream.write(inputBytes, 0, readSize);
        cryptoStream.close();
    }

    public void encrypt(String text, OutputStream outputStream)
            throws Exception {
        byte[] inputBytes = text.getBytes();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBytes);
        byte[] ivBytes = generateIV();
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
        _Cipher.init(Cipher.ENCRYPT_MODE, _SKeySpec, ivParameterSpec);
        CipherInputStream cryptoStream = new CipherInputStream(inputStream,
                _Cipher);
        int readSize;
        while ((readSize = cryptoStream.read(inputBytes, 0,
                FILE_BLOCK_BYTE_SIZE)) > 0)
            outputStream.write(ivBytes, 0, readSize);
        cryptoStream.close();
        inputStream.close();
        outputStream.close();
    }

    // public void Encrypt(Stream inputStream, Stream outputStream)
    // {
    // byte[] ivBytes = generateIV();
    // outputStream.Write(ivBytes, 0, ivBytes.length);
    //
    // ICryptoTransform saEnc = _AESProvider.CreateEncryptor(_KeyBytes,
    // ivBytes);
    // using (CryptoStream cryptoStream = new CryptoStream(outputStream, saEnc,
    // CryptoStreamMode.Write))
    // {
    // int readSize;
    // byte[] inputBytes = new byte[FILE_BLOCK_BYTE_SIZE];
    // while ((readSize = inputStream.Read(inputBytes, 0, FILE_BLOCK_BYTE_SIZE))
    // > 0)
    // cryptoStream.Write(inputBytes, 0, readSize);
    // }
    // }

    // public String Encrypt(String input) {
    // try {
    // byte[] ivBytes = generateIV();
    // ICryptoTransform saEnc = _AESProvider.CreateEncryptor(_KeyBytes,
    // ivBytes);
    // byte[] inputBytes = Encoding.UTF8.GetBytes(input);
    // byte[] resultBytes = saEnc.TransformFinalBlock(inputBytes, 0,
    // inputBytes.Length);
    //
    // byte[] allBytes = new byte[ivBytes.Length + resultBytes.Length];
    // System.Buffer.BlockCopy(ivBytes, 0, allBytes, 0, ivBytes.Length);
    // System.Buffer.BlockCopy(resultBytes, 0, allBytes, ivBytes.Length,
    // resultBytes.Length);
    // return Convert.ToBase64String(allBytes);
    // } catch (Exception ex) {
    // Console.WriteLine(ex);
    // return "";
    // }
    // }

    public void decrypt(InputStream inputStream, OutputStream outputStream) {
        try {
            byte[] ivBytes = new byte[IV_BYTE_SIZE];
            inputStream.read(ivBytes, 0, IV_BYTE_SIZE);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
            _Cipher.init(Cipher.DECRYPT_MODE, _SKeySpec, ivParameterSpec);
            CipherInputStream cryptoStream = new CipherInputStream(inputStream,
                    _Cipher);
            int readSize;
            byte[] inputBytes = new byte[FILE_BLOCK_BYTE_SIZE];
            while ((readSize = cryptoStream.read(inputBytes, 0,
                    FILE_BLOCK_BYTE_SIZE)) > 0)
                outputStream.write(inputBytes, 0, readSize);
            cryptoStream.close();
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            System.out.println("I/O Error:" + e.getMessage());
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            System.out.println("Invalid Kay Error:" + e.getMessage());
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("Invalid Algorithm Parameter Error:"
                    + e.getMessage());
            e.printStackTrace();
        }
        System.gc();
    }

    public String decrypt(InputStream inputStream) throws IOException {
        try {
            byte[] ivBytes = new byte[IV_BYTE_SIZE];
            inputStream.read(ivBytes, 0, IV_BYTE_SIZE);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
            _Cipher.init(Cipher.DECRYPT_MODE, _SKeySpec, ivParameterSpec);
            CipherInputStream cryptoStream = new CipherInputStream(inputStream,
                    _Cipher);
            int readSize;
            byte[] inputBytes = new byte[FILE_BLOCK_BYTE_SIZE];

            StringBuilder x = new StringBuilder();
            while ((readSize = cryptoStream.read(inputBytes, 0,
                    FILE_BLOCK_BYTE_SIZE)) > 0)
                x.append(new String(inputBytes, 0, readSize, "UTF-8"));
//			writeLog.LogE("LOG", x.toString());
            inputStream.close();
            cryptoStream.close();
            System.gc();
            return x.toString();
        } catch (IOException e) {
            System.out.println("I/O Error:" + e.getMessage());
            e.printStackTrace();
            System.gc();
            return null;
        } catch (InvalidKeyException e) {
            System.out.println("Invalid Kay Error:" + e.getMessage());
            e.printStackTrace();
            System.gc();
            return null;
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("Invalid Algorithm Parameter Error:"
                    + e.getMessage());
            e.printStackTrace();
            System.gc();
            return null;
        }
    }

    public String decrypt(String textEncrypt) throws Exception {
        byte[] inputBytes = Base64.decode(textEncrypt, Base64.DEFAULT);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(inputBytes);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        decrypt(inputStream, outputStream);
        byte[] resultBytes = outputStream.toByteArray();
        inputStream.close();
        outputStream.close();
        return new String(_Cipher.doFinal(resultBytes), "UTF-8");
    }

    public void decrypt2(InputStream inputStream, OutputStream outputStream)
            throws Exception {
        byte[] ivBytes = new byte[IV_BYTE_SIZE];
        outputStream.write(ivBytes, 0, ivBytes.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);
        _Cipher.init(Cipher.DECRYPT_MODE, _SKeySpec, ivParameterSpec);
    }

    @SuppressLint("TrulyRandom")
    private byte[] generateIV() throws UnsupportedEncodingException {
        byte[] ivBytes = new byte[IV_BYTE_SIZE];
        SecureRandom random = new SecureRandom();
        random.nextBytes(ivBytes);
        return ivBytes;
    }
}