package com.jorlek.queqforms.services;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class Services {

    public CallApi callApi;
    private Context mContext;

    /* main root PRODUCTION */
    public static String root_type = "PROD";
    private static String baseUrl = "https://api-queq-forms.ymmydev.com/front/"; //Production Leave

    /* main root UAT */
//    public static String root_type = "UAT";
//    private static String baseUrl = "https://api-queq-forms-uat.ymmydev.com/front/";

    private static String queq_forms_cert;

    public ProgressDialog loadingDialog;

    public Services(Context context) {

        mContext = context;

        setCert();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttpClient(context))
                .build();
        callApi = retrofit.create(CallApi.class);

        loadingDialog = new ProgressDialog(mContext);

    }

    private void setCert() {
        if(root_type.equals("PROD"))
            queq_forms_cert = "queq_forms_prod.crt";
        else
            queq_forms_cert = "queq_forms.crt";
    }

    private OkHttpClient getOkHttpClient(Context context) {

        try {
            InputStream inputCertificates = context.getAssets().open(queq_forms_cert);
            SSLCertificates sslCertificates = new SSLCertificates(inputCertificates);
            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslCertificates.getSslSocketFactory(), sslCertificates.getTrustManager())
                    .hostnameVerifier(new HostnameVerifier() {
                        @SuppressLint("BadHostnameVerifier")
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void showDialog(boolean isShowDialog) {
        if (isShowDialog) {
            if(!loadingDialog.isShowing()) {
                loadingDialog.setIndeterminate(true);
                loadingDialog.setCancelable(false);
                loadingDialog.setMessage("Loading...");
                loadingDialog.show();
            }
        }
    }

    public void hideDialog() {
        if (loadingDialog.isShowing())
            loadingDialog.dismiss();
    }

}
