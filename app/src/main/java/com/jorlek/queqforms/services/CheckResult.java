package com.jorlek.queqforms.services;

import com.jorlek.queqforms.AppConfig;

public class CheckResult {

    public static boolean checkSuccess(String code) throws TokenException {
        if (code.equals(AppConfig.CheckServiceResult.SUCCESS)) {
            return true;
        }
        else if (code.equals(AppConfig.CheckServiceResult.SESSION_EXPIRED)) {
            throw new TokenException("token ex");
        }
//        else if (code.equals(APPConfig.REUSLT_E000004)) {
//            APPConfig.ERROR04 = true;
//            throw new TokenException("REUSLT_E000004");
//        }
//        else if (APPConfig.ERROR04) {
//            throw new TokenException("REUSLT_E000004");
//        }
        else {
            return false;
        }
    }
}