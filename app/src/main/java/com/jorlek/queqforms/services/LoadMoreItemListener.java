package com.jorlek.queqforms.services;

/**
 * Created by chillsweet on 11/25/2016 AD.
 */

public interface LoadMoreItemListener {

    void onCallServiceItem(final int page_index, final String callService);

}
