package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.AdapterDynamicRadioListItemBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chillsweet on 12/15/2016 AD.
 */

public class DynamicRadioListItemsAdapter extends RecyclerView.Adapter<DynamicRadioListItemsAdapter.ViewHolder> {

    private Context mContext;

    private List<J_Form_Question_Name_Value> radioItem;

    private ArrayList<String> name, value;

    public DynamicRadioListItemsAdapter(Context mContext, List<J_Form_Question_Name_Value> radioItem) {
        this.mContext = mContext;
        this.radioItem = radioItem;
        name = new ArrayList<>();
        value = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private AdapterDynamicRadioListItemBinding binding;

        public ViewHolder(AdapterDynamicRadioListItemBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(int position) {
            binding.tvName.setText(radioItem.get(position).getName());
            name.add(radioItem.get(position).getName());
            value.add(radioItem.get(position).getVal());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AdapterDynamicRadioListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_dynamic_radio_list_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return radioItem.size();
    }

    public String getName(int position) {
        return name.get(position);
    }

    public String getVal(int position) {
        return value.get(position);
    }

}
