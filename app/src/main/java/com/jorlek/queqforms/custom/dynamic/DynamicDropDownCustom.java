package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicDropDownBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicDropDownCustom extends DynamicFieldCustom implements View.OnClickListener {

    private DynamicDropDownBinding binding;

    private M_formQuestionList mFormQuestionList;

    public DynamicDropDownCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_drop_down, null, false);

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setOnClick();

        addView(binding.getRoot());
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.tvInput.setText(defaultItem.get(0).getVal());
            }
        }
    }

    private void setOnClick() {
        binding.btnDropDown.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_drop_down:

                break;
        }
    }

//    private void showDialog() {
//
//        List<J_Form_Question_Name_Value> dropDownItem = Global.convertGsonModel(mFormQuestionList);
//
//        final ArrayList<String> listName = new ArrayList<>();
//        final ArrayList<String> listVal = new ArrayList<>();
//        for (int i = 0; i < dropDownItem.size(); i++) {
//            listName.add(dropDownItem.get(i).getName());
//            listVal.add(dropDownItem.get(i).getVal());
//        }
//
//        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
//        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.dialog_alert, listVal);
//        alertDialog.setNegativeButton(
//                "cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int position) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.setAdapter(
//                arrayAdapter,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int position) {
//                        binding.tvInput.setText(listName.get(position));
//                        setText(listVal.get(position));
//                        setName(listName.get(position));
//                        setVal(listVal.get(position));
//                    }
//                });
//        alertDialog.show();
//    }
}
