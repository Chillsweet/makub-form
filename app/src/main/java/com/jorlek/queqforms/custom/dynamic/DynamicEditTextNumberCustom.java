package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicEditTextBinding;
import com.jorlek.queqforms.databinding.DynamicEditTextNumberBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.List;

/**
 * Created by chillsweet on 12/16/2016AD.
 */

public class DynamicEditTextNumberCustom extends DynamicFieldCustom {

    private DynamicEditTextNumberBinding binding;

    private M_formQuestionList mFormQuestionList;

    public DynamicEditTextNumberCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_edit_text_number, null, false);

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setTextChangeListener();

        setEditText(binding.edtDynamic);

        addView(binding.getRoot());

    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.edtDynamic.setHint(defaultItem.get(0).getVal());
            }
        }
    }

    private void setTextChangeListener() {
        binding.edtDynamic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setText(editable.toString());
            }
        });
    }
}
