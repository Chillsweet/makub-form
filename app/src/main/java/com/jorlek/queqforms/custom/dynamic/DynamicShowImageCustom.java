package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicShowImageBinding;
import com.jorlek.queqforms.databinding.DynamicTextViewBinding;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.squareup.picasso.Picasso;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicShowImageCustom extends DynamicFieldCustom {

    private DynamicShowImageBinding binding;

    public DynamicShowImageCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_show_image, null, false);

        writeLog.LogE("DynamicShowImageCustom", mFormQuestionList.getForm_question_default());

        Picasso.with(context).load(mFormQuestionList.getForm_question_default()).into(binding.imgDynamic);

        addView(binding.getRoot());
    }

}
