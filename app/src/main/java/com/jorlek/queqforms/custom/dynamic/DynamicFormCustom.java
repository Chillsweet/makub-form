package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicFormBinding;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.services.Services;

import java.util.ArrayList;

/**
 * Created by chillsweet on 12/15/2016 AD.
 */

public class DynamicFormCustom extends LinearLayout {

    private Context mContext;

    private DynamicFormBinding binding;

    private M_formQuestionList mFormQuestionList;

    private DynamicFieldCustom dynamicFieldCustom;

    public DynamicFormCustom(Context mContext, M_formQuestionList mFormQuestionList) {
        super(mContext);
        this.mContext = mContext;
        this.mFormQuestionList = mFormQuestionList;
        buildInputLayout();
    }

    private void buildInputLayout() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_form, null, false);
        binding.tvTitle.setText(mFormQuestionList.getForm_question_title());
        if (mFormQuestionList.isForm_question_require_field()) {
            binding.tvRequire.setVisibility(View.VISIBLE);
        }
        else {
            binding.tvRequire.setVisibility(View.GONE);
        }
        getView();
        addView(binding.getRoot());
    }

    public void getView() {
        switch (mFormQuestionList.getInput_type_id()) {

            //Text Box
            case 1:
                addEditText();
                break;

            //Text Area
            case 2:
                addTextArea();
                break;

            //Drop Down
            case 3:
                addDropDown();
                break;

            //Date Time
            case 4:
                addDateTime();
                break;

            //Date
            case 5:
                addDate();
                break;

            //Time
            case 6:
                addTime();
                break;

            //Radio
            case 7:
                addRadioGroup();
                break;

            //Check Box
            case 8:
                addCheckBox();
                break;

            //Signature
            case 9:
                addSignature();
                break;

            //Scan Device
            case 10:
                addEditTextDevice();
                break;

            //Scan IMEI
            case 11:
                addEditTextIMEI();
                break;

            //Radio TextView
            case 12:
                addRadioNewPage();
                break;

            //Number
            case 13:
                addEditTextNumber();
                break;

            //Decimal
            case 14:
                addEditTextDecimal();
                break;

            //Text Hidden
            case 15:
                addTextHiddenView();
                break;

            //BirthDate Hidden
            case 16:
                addTextHiddenView();
                break;

            //ID Number
            case 17:
                addEditTextIDNumber();
                break;

            //TextView
            case 999:
                addTextView();
                break;

            default:
                binding.llDynamicForm.setVisibility(GONE);
                break;
        }
    }

    private void addSignature() {
        dynamicFieldCustom = new DynamicSignatureCustom(mContext);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addDropDown() {
        dynamicFieldCustom = new DynamicDropDownCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditText() {
        dynamicFieldCustom = new DynamicEditTextCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditTextNumber() {
        dynamicFieldCustom = new DynamicEditTextNumberCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditTextDecimal() {
        dynamicFieldCustom = new DynamicEditTextDecimalCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditTextIMEI() {
        dynamicFieldCustom = new DynamicEditTextIMEICustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditTextDevice() {
        dynamicFieldCustom = new DynamicEditTextDeviceCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addEditTextIDNumber() {
        dynamicFieldCustom = new DynamicEditTextIDNumberCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addTextArea() {
        dynamicFieldCustom = new DynamicTextAreaCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addDate() {
        dynamicFieldCustom = new DynamicDateCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addTime() {
        dynamicFieldCustom = new DynamicTimeCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addDateTime() {
        dynamicFieldCustom = new DynamicDateTimeCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addRadioGroup() {
        dynamicFieldCustom = new DynamicRadioGroupCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addRadioNewPage() {
        dynamicFieldCustom = new DynamicRadioNewPageCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addCheckBox() {
        dynamicFieldCustom = new DynamicCheckBoxCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addTextHiddenView() {
        binding.llDynamicForm.setVisibility(GONE);
        dynamicFieldCustom = new DynamicTextViewCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addTextView() {
        dynamicFieldCustom = new DynamicTextViewCustom(mContext, mFormQuestionList);
        setFormData();
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void setFormData() {
        dynamicFieldCustom.setQuestionID(mFormQuestionList.getForm_question_id());
        dynamicFieldCustom.setTitle(mFormQuestionList.getForm_question_title());
        dynamicFieldCustom.setRequired(mFormQuestionList.isForm_question_require_field());
        dynamicFieldCustom.setQuestionType(mFormQuestionList.getInput_type_id());
    }

    public String getInput() {
        return dynamicFieldCustom.getText();
    }

    public void setInput(String input) {
        dynamicFieldCustom.setText(input);
    }

    public String getName() {
        return dynamicFieldCustom.getName();
    }

    public void setName(String name) {
        dynamicFieldCustom.setName(name);
    }

    public String getVal() {
        return dynamicFieldCustom.getVal();
    }

    public void setVal(String val) {
        dynamicFieldCustom.setVal(val);
    }

    public ArrayList<DynamicFieldCustom> getDynamicFieldCustomArrayList() {
        return dynamicFieldCustom.getDynamicFieldCustomArrayList();
    }

    public EditText getEditText() {
        return dynamicFieldCustom.getEditText();
    }

    public TextView getTextView() {
        return dynamicFieldCustom.getTextView();
    }

    public boolean isSign() {
        return dynamicFieldCustom.isSign();
    }

    public String getTitle() {
        return dynamicFieldCustom.getTitle();
    }

    public String getQuestionID() {
        return  dynamicFieldCustom.getQuestionID();
    }

    public int getQuestionType() {
        return dynamicFieldCustom.getQuestionType();
    }

    public boolean isRequired() {
        return dynamicFieldCustom.isRequired();
    }

}
