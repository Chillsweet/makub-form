package com.jorlek.queqforms.custom;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.dynamic.DynamicFormHistoryCustom;
import com.jorlek.queqforms.databinding.DialogDubBinding;
import com.jorlek.queqforms.databinding.DialogSuccessBinding;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.models.response.M_reqCheckDub;
import com.jorlek.queqforms.models.response.M_reqCreateFormByEmp;

/**
 * Created by chillsweet on 4/17/2017 AD.
 */

public class DialogSuccess {

    private DialogSuccessBinding binding;

    private Dialog dialog;
    private Context context;
    private M_reqCreateFormByEmp mReqCreateFormByEmp;

    public DialogSuccess(Context context, M_reqCreateFormByEmp mReqCreateFormByEmp) {
        this.context = context;
        this.dialog = new Dialog(context);
        this.mReqCreateFormByEmp = mReqCreateFormByEmp;
    }

    public void showDialog(View.OnClickListener onClickClose) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_success, null, false);
        setSuccessMessage();
        setOnClick(onClickClose);
        setDialog();
        dialog.show();
    }

    private void setSuccessMessage() {
        binding.tvSuccessTitle.setText(mReqCreateFormByEmp.getPass_message());
    }

    private void setOnClick(View.OnClickListener onClickClose) {
        binding.btnClose.setOnClickListener(onClickClose);
    }

    private void setDialog() {
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
    }

    public void dismissDialog() {
        dialog.dismiss();
    }
}