package com.jorlek.queqforms.custom.dynamic;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicDateTimeBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.Calendar;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicDateTimeCustom extends DynamicFieldCustom implements View.OnClickListener {

    private DynamicDateTimeBinding binding;

    private Context context;

    private M_formQuestionList mFormQuestionList;

    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;

    private String[] month;
    private String date, time;
    private int selectDay, selectMonth, selectYear;
    private int selectHour, selectMinute;

    public DynamicDateTimeCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_date_time, null, false);

        this.context = context;

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setOnClick();

        setCalendar();

        setDatePickerDialog();

        setTimePickerDialog();

        addView(binding.getRoot());
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.tvInput.setText(defaultItem.get(0).getVal());
            }
        }
    }

    private void setOnClick() {
        binding.btnDateTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_date_time:
                datePickerDialog.show();
                break;
        }
    }

    private void setCalendar() {
        Calendar cal = Calendar.getInstance();
        selectHour = cal.get(Calendar.HOUR_OF_DAY);
        selectMinute = cal.get(Calendar.MINUTE);
        selectDay = cal.get(Calendar.DAY_OF_MONTH);
        selectMonth = cal.get(Calendar.MONTH);
        selectYear = cal.get(Calendar.YEAR);
    }

    private void setDatePickerDialog() {
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int yearOfYear, int monthOfYear, int dayOfYear) {
                selectDay = dayOfYear;
                selectMonth = monthOfYear;
                selectYear = yearOfYear;
                setDateTime();
                timePickerDialog.show();
            }
        }, selectYear, selectMonth, selectDay);
    }

    private void setTimePickerDialog() {
        timePickerDialog = new TimePickerDialog(getContext(), TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                selectHour = hour;
                selectMinute = minute;
                setDateTime();
            }
        }, selectHour, selectMinute, true);
    }

    private void setDateTime() {
        date = Global.buildDateFormat(selectDay, selectMonth, selectYear, Prefs.getLanguage(context));
        time = Global.buildTimeFormat(selectHour, selectMinute);
        binding.tvInput.setText(date + " " + time);
        setText(date + " " + time);
    }
}
