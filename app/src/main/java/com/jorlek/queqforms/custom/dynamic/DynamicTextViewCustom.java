package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicTextViewBinding;
import com.jorlek.queqforms.models.response.M_formQuestionList;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicTextViewCustom extends DynamicFieldCustom {

    private DynamicTextViewBinding binding;

    public DynamicTextViewCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_text_view, null, false);

        binding.tvDynamic.setText(mFormQuestionList.getForm_question_default());

        setTextView(binding.tvDynamic);

        setText(mFormQuestionList.getForm_question_default());

        addView(binding.getRoot());
    }

}
