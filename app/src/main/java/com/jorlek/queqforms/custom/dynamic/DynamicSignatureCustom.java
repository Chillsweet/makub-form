package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.DialogSign;
import com.jorlek.queqforms.databinding.DynamicDropDownBinding;
import com.jorlek.queqforms.databinding.DynamicSignatureBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicSignatureCustom extends DynamicFieldCustom implements View.OnClickListener {

    private DynamicSignatureBinding binding;

    private Context context;

    private DialogSign dialogSign;

    public DynamicSignatureCustom(Context context) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_signature, null, false);

        this.context = context;

        setOnClick();

        addView(binding.getRoot());
    }

    private void setOnClick() {
        binding.btnSign.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_sign:
                doBtnSign();
                break;
        }
    }

    private void doBtnSign() {
        dialogSign = new DialogSign(context);
        dialogSign.showDialog(view -> {
            dialogSign.saveImageSign();
            dialogSign.dismissDialog();
            showImageSign();
        });
    }

    private void showImageSign() {
        setSign(dialogSign.isSign());
        if(isSign()) {
            binding.imgSign.setVisibility(View.VISIBLE);
            binding.llBtnSign.setVisibility(View.GONE);
            Global.setImageFromInternalStorage(context, binding.imgSign, "sign.png");
        }
        else {
            binding.imgSign.setVisibility(View.GONE);
            binding.llBtnSign.setVisibility(View.VISIBLE);
        }
    }

}
