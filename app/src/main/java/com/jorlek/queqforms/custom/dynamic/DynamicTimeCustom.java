package com.jorlek.queqforms.custom.dynamic;

import android.app.TimePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicTimeBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.Calendar;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicTimeCustom extends DynamicFieldCustom implements View.OnClickListener {

    private DynamicTimeBinding binding;

    private M_formQuestionList mFormQuestionList;

    private TimePickerDialog timePickerDialog;

    private int selectHour, selectMinute;

    public DynamicTimeCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_time, null, false);

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setOnClick();

        setCalendar();

        addView(binding.getRoot());
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.tvInput.setText(defaultItem.get(0).getVal());
            }
        }
    }

    private void setOnClick() {
        binding.btnTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_time:
                setTimePickerDialog();
                timePickerDialog.show();
                break;
        }
    }

    private void setCalendar() {
        Calendar mCurrentTime = Calendar.getInstance();
        selectHour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        selectMinute = mCurrentTime.get(Calendar.MINUTE);
    }

    private void setTimePickerDialog() {
        timePickerDialog = new TimePickerDialog(getContext(), TimePickerDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                selectHour = hour;
                selectMinute = minute;
                setTime();
            }
        }, selectHour, selectMinute, true);
    }

    private void setTime() {
        binding.tvInput.setText(Global.buildTimeFormat(selectHour, selectMinute));
        setText(binding.tvInput.getText().toString());
    }
}
