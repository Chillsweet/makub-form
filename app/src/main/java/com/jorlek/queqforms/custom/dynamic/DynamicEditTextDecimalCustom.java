package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicEditTextBinding;
import com.jorlek.queqforms.databinding.DynamicEditTextDecimalBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by chillsweet on 12/16/2016AD.
 */

public class DynamicEditTextDecimalCustom extends DynamicFieldCustom {

    private DynamicEditTextDecimalBinding binding;

    private M_formQuestionList mFormQuestionList;

    public DynamicEditTextDecimalCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_edit_text_decimal, null, false);

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setTextChangeListener();

        setEditText(binding.edtDynamic);

        addView(binding.getRoot());

    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.edtDynamic.setHint(defaultItem.get(0).getVal());
            }
        }
    }

    private void setTextChangeListener() {
        binding.edtDynamic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                binding.edtDynamic.removeTextChangedListener(this);

                try {
                    String givenstring = s.toString();
                    Long longval;
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replaceAll(",", "");
                    }
                    longval = Long.parseLong(givenstring);
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString = formatter.format(longval);
                    binding.edtDynamic.setText(formattedString);
                    binding.edtDynamic.setSelection(binding.edtDynamic.getText().length());
                    // to place the cursor at the end of text
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                binding.edtDynamic.addTextChangedListener(this);

                setText(binding.edtDynamic.getText().toString());
            }
        });
    }
}
