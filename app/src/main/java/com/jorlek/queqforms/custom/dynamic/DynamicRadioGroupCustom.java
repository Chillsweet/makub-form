package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicRadioGroupBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicRadioGroupCustom extends DynamicFieldCustom {

    private Context mContext;

    private DynamicRadioGroupBinding binding;

    private M_formQuestionList mFormQuestionList;

    final ArrayList<String> listName = new ArrayList<>();
    final ArrayList<String> listVal = new ArrayList<>();

    public DynamicRadioGroupCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_radio_group, null, false);

        mContext = context;

        this.mFormQuestionList = mFormQuestionList;

        setRadioButton();

        setRadioOnCheck();

        addView(binding.getRoot());
    }

    private void setRadioOnCheck() {
        binding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            // checkedId is the RadioButton selected
            RadioButton radioButtonCheck =(RadioButton)findViewById(checkedId);
            setText(listVal.get(Integer.parseInt(radioButtonCheck.getTag().toString())));
            setName(listName.get(Integer.parseInt(radioButtonCheck.getTag().toString())));
            setVal(listVal.get(Integer.parseInt(radioButtonCheck.getTag().toString())));
        });
    }

    public void setRadioButton() {

        List<J_Form_Question_Name_Value> radioItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());

        for (int i = 0; i < radioItem.size(); i++) {
            listName.add(radioItem.get(i).getName());
            listVal.add(radioItem.get(i).getVal());
            RadioButton radioButton = (RadioButton) LayoutInflater.from(mContext).inflate(R.layout.dynamic_radio_button, null);
            radioButton.setTag(i);
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.tv_headline));
            radioButton.setText(listVal.get(i));
            binding.radioGroup.addView(radioButton);
        }
    }
}
