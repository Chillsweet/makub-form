package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicFieldCustom extends LinearLayout {

    private String input = "";
    private String name = "";
    private String val = "";
    private ArrayList<DynamicFieldCustom> dynamicFieldCustomArrayList = new ArrayList<>();
    private EditText editText;
    private TextView textView;
    private boolean isSign;
    private String title = "";
    private String questionID = "";
    private int questionType;
    private boolean isRequired;

    public DynamicFieldCustom(Context context) {
        super(context);

    }

    public void setText(String text) {
        input = text;
    }

    public String getText() {
        return input;
    }

    public void setName(String text) {
        name = text;
    }

    public String getName() {
        return name;
    }

    public void setVal(String text) {
        val = text;
    }

    public String getVal() {
        return val;
    }

    public ArrayList<DynamicFieldCustom> getDynamicFieldCustomArrayList() {
        return dynamicFieldCustomArrayList;
    }

    public void setDynamicFieldCustomArrayList(ArrayList<DynamicFieldCustom> dynamicFieldCustomArrayList) {
        this.dynamicFieldCustomArrayList = dynamicFieldCustomArrayList;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public boolean isSign() {
        return isSign;
    }

    public void setSign(boolean sign) {
        isSign = sign;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }
}