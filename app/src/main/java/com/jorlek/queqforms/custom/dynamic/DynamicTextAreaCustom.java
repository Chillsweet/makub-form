package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicTextAreaBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicTextAreaCustom extends DynamicFieldCustom {

    private DynamicTextAreaBinding binding;

    private M_formQuestionList mFormQuestionList;

    public DynamicTextAreaCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_text_area, null, false);

        this.mFormQuestionList = mFormQuestionList;

        setDefaultValue();

        setTextChangeListener();

        addView(binding.getRoot());
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.edtTextArea.setHint(defaultItem.get(0).getVal());
            }
        }
    }

    private void setTextChangeListener() {
        binding.edtTextArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setText(editable.toString());
            }
        });
    }
}
