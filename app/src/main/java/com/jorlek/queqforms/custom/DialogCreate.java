package com.jorlek.queqforms.custom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.jorlek.queqforms.login.LoginActivity;
import com.jorlek.queqforms.registration.RegistrationActivity;


public class DialogCreate {
    ProgressDialog dialog;
    Context context;

    public DialogCreate(Context context) {
        this.context = context;
        dialog = new ProgressDialog(context);
    }

    public void DialogDismiss() {
        if (this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    public void AlertForNetWork(CharSequence charSequence) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        alert.setMessage(charSequence);
        alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (context instanceof RegistrationActivity) {
                    DialogDismiss();
                    System.exit(0);
                    ((Activity) context).finish();
                }
                else {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(0, 0);
                    context.startActivity(intent);
                }
            }
        });
        alert.show();
    }

    public void AlertError(CharSequence charSequence, CharSequence textButton, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        alert.setMessage(charSequence);
        alert.setNegativeButton(textButton, listener);
        alert.show();
    }

}