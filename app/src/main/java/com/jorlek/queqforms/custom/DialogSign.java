package com.jorlek.queqforms.custom;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DialogSignBinding;
import com.rm.freedrawview.PathDrawnListener;

/**
 * Created by chillsweet on 4/17/2017 AD.
 */

public class DialogSign {

    private DialogSignBinding binding;

    private Dialog dialog;
    private Context context;

    private boolean isUserSign = false;

    public DialogSign(Context context) {
        this.context = context;
        this.dialog = new Dialog(context);
    }

    public void showDialog(View.OnClickListener onClickSave) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_sign, null, false);
        setOnClick(onClickSave);
        setFreeDrawOnTouch();
        setDialog();
        dialog.show();
    }

    private void setFreeDrawOnTouch() {
        binding.freeDraw.setOnPathDrawnListener(new PathDrawnListener() {
            @Override
            public void onPathStart() {
                isUserSign = true;
            }

            @Override
            public void onNewPathDrawn() {
                isUserSign = true;
            }
        });
    }

    private void setOnClick(View.OnClickListener onClickSave) {
        binding.btnClose.setOnClickListener(view -> dialog.dismiss());

        binding.btnSave.setOnClickListener(onClickSave);

        binding.btnDelete.setOnClickListener(view -> {
            isUserSign = false;
            binding.freeDraw.undoAll();
        });
    }

    public void saveImageSign() {
        binding.rlSign.setDrawingCacheEnabled(true);
        Global.saveToInternalStorage(context, binding.rlSign.getDrawingCache(), "sign.png");
    }

    private void setDialog() {
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setOnKeyListener((arg0, keyCode, event) -> {
            // TODO Auto-generated method stub
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dialog.dismiss();
            }
            return true;
        });
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
    }

    public void dismissDialog() {
        dialog.dismiss();
    }

    public boolean isSign() {
        return isUserSign;
    }
}