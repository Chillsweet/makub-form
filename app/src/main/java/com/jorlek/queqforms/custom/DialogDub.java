package com.jorlek.queqforms.custom;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.dynamic.DynamicFormHistoryCustom;
import com.jorlek.queqforms.databinding.DialogDubBinding;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.models.response.M_reqCheckDub;

/**
 * Created by chillsweet on 4/17/2017 AD.
 */

public class DialogDub {

    private DialogDubBinding binding;

    private Dialog dialog;
    private Context context;
    private M_reqCheckDub mReqCheckDub;

    public DialogDub(Context context, M_reqCheckDub mReqCheckDub) {
        this.context = context;
        this.dialog = new Dialog(context);
        this.mReqCheckDub = mReqCheckDub;
    }

    public void showDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_dub, null, false);
        setOnClick();
        setDialog();
        setFormView();
        dialog.show();
    }

    private void setOnClick() {
        binding.btnClose.setOnClickListener(view -> dialog.dismiss());
    }

    private void setDialog() {
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setOnKeyListener((arg0, keyCode, event) -> {
            // TODO Auto-generated method stub
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dialog.dismiss();
            }
            return true;
        });
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
    }

    private void setFormView() {
        binding.tvDubTitle.setText(mReqCheckDub.getTitle());
        for (M_formQuestionList formQuestionList : mReqCheckDub.getForm_question_list()) {
            binding.containerForm.addView(new DynamicFormHistoryCustom(context, formQuestionList));
        }
    }

    public void dismissDialog() {
        dialog.dismiss();
    }
}