package com.jorlek.queqforms.custom;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DialogConfirmBinding;
import com.jorlek.queqforms.databinding.DialogSuccessBinding;
import com.jorlek.queqforms.models.response.M_reqCreateFormByEmp;

/**
 * Created by chillsweet on 4/17/2017 AD.
 */

public class DialogConfirm {

    private DialogConfirmBinding binding;

    private Dialog dialog;
    private Context context;

    public DialogConfirm(Context context) {
        this.context = context;
        this.dialog = new Dialog(context);
    }

    public void showDialog(View.OnClickListener onClickOK) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_confirm, null, false);
        setOnClick(onClickOK);
        setDialog();
        dialog.show();
    }

    private void setOnClick(View.OnClickListener onClickOK) {
        binding.btnOk.setOnClickListener(onClickOK);
        binding.btnCancel.setOnClickListener(view -> dialog.dismiss());
    }

    private void setDialog() {
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(false);
    }

    public void dismissDialog() {
        dialog.dismiss();
    }
}