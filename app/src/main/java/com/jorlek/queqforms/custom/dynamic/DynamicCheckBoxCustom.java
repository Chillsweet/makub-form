package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicCheckBoxBinding;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicCheckBoxCustom extends DynamicFieldCustom {

    private Context mContext;

    private DynamicCheckBoxBinding binding;

    private M_formQuestionList mFormQuestionList;

    private ArrayList<DynamicFieldCustom> dynamicFieldCustomArrayList = new ArrayList<>();

    final ArrayList<String> listName = new ArrayList<>();
    final ArrayList<String> listVal = new ArrayList<>();

    int tagDefault = -1;

    public DynamicCheckBoxCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_check_box, null, false);

        mContext = context;

        this.mFormQuestionList = mFormQuestionList;

        setOnClick();

        setDefaultValue();

        addView(binding.getRoot());
    }

    private void setOnClick() {
        binding.btnAdd.setOnClickListener(view -> doBtnGiftAdd());
    }

    private void setDefaultValue() {
        if (mFormQuestionList.getForm_question_default() != null) {
            setCheckBoxItem();
        }
        else {
            tagDefault = 0;
            doBtnGiftAdd();
        }
    }

    private void setCheckBoxItem() {

        List<J_Form_Question_Name_Value> checkBoxItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
        tagDefault = checkBoxItem.size();
        for (int i = 0; i < checkBoxItem.size(); i++) {
            listName.add(checkBoxItem.get(i).getName());
            listVal.add(checkBoxItem.get(i).getVal());
            CheckBox checkBox = (CheckBox) LayoutInflater.from(mContext).inflate(R.layout.dynamic_check_box_item, null);
            checkBox.setTag(i);
            checkBox.setText(checkBoxItem.get(i).getVal());
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.tv_headline));
            checkBox.setOnCheckedChangeListener(checkBoxListener);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 8); //substitute parameters for left, top, right, bottom
            checkBox.setLayoutParams(params);
            binding.containerCheckBox.addView(checkBox);
        }
    }

    private void doBtnGiftAdd() {

        binding.btnAdd.setVisibility(GONE);

        List<J_Form_Question_Name_Value> checkBoxItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default_nd());

        for (int i = 0; i < checkBoxItem.size(); i++) {
            listName.add(checkBoxItem.get(i).getName());
            listVal.add(checkBoxItem.get(i).getVal());
            CheckBox checkBox = (CheckBox) LayoutInflater.from(mContext).inflate(R.layout.dynamic_check_box_item, null);
            checkBox.setTag(tagDefault + i);
            checkBox.setText(checkBoxItem.get(i).getVal());
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.tv_headline));
            checkBox.setOnCheckedChangeListener(checkBoxListener);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 8); //substitute parameters for left, top, right, bottom
            checkBox.setLayoutParams(params);
            binding.containerCheckBox.addView(checkBox);
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBoxListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                DynamicFieldCustom item = new DynamicFieldCustom(mContext);
                writeLog.LogE("onCheckChanged", "getTag: " + Integer.parseInt(buttonView.getTag().toString()));
                item.setText(listVal.get(Integer.parseInt(buttonView.getTag().toString())));
                item.setName(listName.get(Integer.parseInt(buttonView.getTag().toString())));
                item.setVal(listVal.get(Integer.parseInt(buttonView.getTag().toString())));
                dynamicFieldCustomArrayList.add(item);
                setDynamicFieldCustomArrayList(dynamicFieldCustomArrayList);
            }
            else {
                for (int i = 0; i < dynamicFieldCustomArrayList.size(); i++) {
                    if (listVal.get(Integer.parseInt(buttonView.getTag().toString())).equals(dynamicFieldCustomArrayList.get(i).getVal())) {
                        dynamicFieldCustomArrayList.remove(i);
                    }
                }
                setDynamicFieldCustomArrayList(dynamicFieldCustomArrayList);
            }
        }
    };
}
