package com.jorlek.queqforms.custom.dynamic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicFormBinding;
import com.jorlek.queqforms.databinding.DynamicFormHistoryBinding;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.ArrayList;

/**
 * Created by chillsweet on 12/15/2016 AD.
 */

public class DynamicFormHistoryCustom extends LinearLayout {

    private Context mContext;

    private DynamicFormHistoryBinding binding;

    private M_formQuestionList mFormQuestionList;

    private DynamicFieldCustom dynamicFieldCustom;

    public DynamicFormHistoryCustom(Context mContext, M_formQuestionList mFormQuestionList) {
        super(mContext);
        this.mContext = mContext;
        this.mFormQuestionList = mFormQuestionList;
        buildInputLayout();
    }

    private void buildInputLayout() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_form_history, null, false);
        binding.tvTitle.setText(mFormQuestionList.getForm_question_title());
        getView();
        addView(binding.getRoot());
    }

    public void getView() {
        switch (mFormQuestionList.getInput_type_id()) {

            //Signature
            case 998:
                addSignature();
                break;

            //TextView
            case 999:
                addTextView();
                break;
        }
    }

    private void addSignature() {
        dynamicFieldCustom = new DynamicShowImageCustom(mContext, mFormQuestionList);
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void addTextView() {
        dynamicFieldCustom = new DynamicTextViewCustom(mContext, mFormQuestionList);
        binding.layoutInput.addView(dynamicFieldCustom, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public String getInput() {
        return dynamicFieldCustom.getText();
    }

    public String getName() {
        return dynamicFieldCustom.getName();
    }

    public String getVal() {
        return dynamicFieldCustom.getVal();
    }

    public ArrayList<DynamicFieldCustom> getDynamicFieldCustomArrayList() {
        return dynamicFieldCustom.getDynamicFieldCustomArrayList();
    }

}
