package com.jorlek.queqforms.custom.dynamic;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicRadioListBinding;
import com.jorlek.queqforms.helper.RecyclerItemClickListener;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicRadioListCustom extends BaseActivity {

    private DynamicRadioListBinding binding;

    M_formQuestionList mFormQuestionList;

    DynamicRadioListItemsAdapter dynamicRadioListItemsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.dynamic_radio_list);

        getBundleExtras();

        setTopBar();

        setRadioItem();

        setOnClick();

    }

    private void getBundleExtras() {
        Bundle extras = getIntent().getExtras();
        mFormQuestionList = extras.getParcelable("mFormQuestionList");
    }

    private void setOnClick() {
        binding.rvList.addOnItemTouchListener(new RecyclerItemClickListener(DynamicRadioListCustom.this, (view, position) -> {
            Intent intent = new Intent();
            intent.putExtra("form_question_id", mFormQuestionList.getForm_question_id());
            intent.putExtra("name", dynamicRadioListItemsAdapter.getName(position));
            intent.putExtra("val", dynamicRadioListItemsAdapter.getVal(position));
            setResult(RESULT_OK, intent);
            previousActivity();
        }));
    }

    private void setRadioItem() {
        List<J_Form_Question_Name_Value> radioItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
        dynamicRadioListItemsAdapter = new DynamicRadioListItemsAdapter(this, radioItem);
        binding.rvList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvList.setAdapter(dynamicRadioListItemsAdapter);
    }

    private void setTopBar() {
        binding.includeTopBar.tvTitle.setText(getString(R.string.store_list_title));
        binding.includeTopBar.btnMenu.setVisibility(View.GONE);
    }
}
