package com.jorlek.queqforms.custom.dynamic;

import android.app.DatePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicDateBinding;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.Calendar;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicDateCustom extends DynamicFieldCustom implements View.OnClickListener {

    private DynamicDateBinding binding;

    private Context context;

    private M_formQuestionList mFormQuestionList;

    private DatePickerDialog datePickerDialog;

    private String[] month;
    private int selectDate, selectMonth, selectYear;

    public DynamicDateCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_date, null, false);

        this.context = context;

        this.mFormQuestionList = mFormQuestionList;
        
        setDefaultValue();

        setOnClick();

        setCalendar();

        setDatePickerDialog();

        addView(binding.getRoot());
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.tvInput.setText(defaultItem.get(0).getVal());
            }
        }
    }

    private void setOnClick() {
        binding.btnDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_date:
                datePickerDialog.show();
                break;
        }
    }

    private void setCalendar() {
        Calendar cal = Calendar.getInstance();
        selectDate = cal.get(Calendar.DAY_OF_MONTH);
        selectMonth = cal.get(Calendar.MONTH);
        selectYear = cal.get(Calendar.YEAR);
    }

    private void setDate() {
        binding.tvInput.setText(Global.buildDateFormat(selectDate, selectMonth, selectYear, Prefs.getLanguage(context)));
        setText(Global.buildDateFormat(selectDate, selectMonth, selectYear, Prefs.getLanguage(context)));
    }

    private void setDatePickerDialog() {
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int yearOfYear, int monthOfYear, int dayOfYear) {
                selectDate = dayOfYear;
                selectMonth = monthOfYear;
                selectYear = yearOfYear;
                setDate();
            }
        }, selectYear, selectMonth, selectDate);
    }
}
