package com.jorlek.queqforms.custom.dynamic;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;

import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.DialogDub;
import com.jorlek.queqforms.databinding.DynamicEditTextBinding;
import com.jorlek.queqforms.helper.writeLog;
import com.jorlek.queqforms.main.MainActivity;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.request.J_reqCheckCarddub;
import com.jorlek.queqforms.models.request.J_reqCheckIMEIdub;
import com.jorlek.queqforms.models.response.M_formQuestionList;
import com.jorlek.queqforms.models.response.M_reqCheckDub;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.Services;
import com.jorlek.queqforms.services.TokenException;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chillsweet on 12/16/2016AD.
 */

public class DynamicEditTextIDNumberCustom extends DynamicFieldCustom {

    private DynamicEditTextBinding binding;

    private Activity context;

    private M_formQuestionList mFormQuestionList;

    private Services services;

    public DynamicEditTextIDNumberCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_edit_text, null, false);

        this.context = (Activity) context;

        this.mFormQuestionList = mFormQuestionList;

        services = new Services(context);

        setEditText(binding.edtDynamic);

        setDefaultValue();

        setTextChangeListener();

        setActionDone();

        addView(binding.getRoot());

    }

    private void setActionDone() {
        binding.edtDynamic.setOnEditorActionListener(
                (v, actionId, event) -> {
                    // Identifier of the action. This will be either the identifier you supplied,
                    // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                    if (actionId == EditorInfo.IME_ACTION_SEARCH
                            || actionId == EditorInfo.IME_ACTION_DONE
                            || event.getAction() == KeyEvent.ACTION_DOWN
                            && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        setText(binding.edtDynamic.getText().toString());
                        callReqCheckIdCarddub(binding.edtDynamic.getText().toString(), true);
                        Global.hideSoftKeyboard(context, binding.edtDynamic);
                        return true;
                    }
                    // Return true if you have consumed the action, else false.
                    return false;
                });
    }

    private void setDefaultValue() {
        if(!Global.isStringEmpty(mFormQuestionList.getForm_question_default())) {
            List<J_Form_Question_Name_Value> defaultItem = Global.convertGsonModel(mFormQuestionList.getForm_question_default());
            if(defaultItem.size() != 0) {
                binding.edtDynamic.setHint(defaultItem.get(0).getVal());
            }
        }
    }

    private void setTextChangeListener() {
        binding.edtDynamic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setText(editable.toString());
            }
        });
    }

    private void callReqCheckIdCarddub(String id_card, boolean isShowDialog) {
        if (Global.isNetworkOnline(context)) {
            services.showDialog(isShowDialog);
            J_reqCheckCarddub jReqCheckCarddub = new J_reqCheckCarddub(id_card);
            Observable<M_reqCheckDub> callReqCheckIdCarddubApi = services.callApi.reqCheckIdCarddub(Prefs.getToken(context), jReqCheckCarddub);
            callReqCheckIdCarddubApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqCheckDub>() {
                        @Override
                        public void onNext(M_reqCheckDub mReqCheckDub) {
                            if (mReqCheckDub != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqCheckDub.getResult_code())) {
                                        checkCarddub(mReqCheckDub);
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(context);
                                }
                            }
                            else Global.showTimeOutDialog(context);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqCheckIdCarddub", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private void checkCarddub(M_reqCheckDub mReqCheckDub) {
        if (mReqCheckDub.isDub_flag()) {
            DialogDub dialogDub = new DialogDub(context, mReqCheckDub);
            dialogDub.showDialog();
//            for (int i = 0; i < 3; i++) {
//                DynamicFormCustom dynamicFormCustom = (DynamicFormCustom) binding.containerForm.getChildAt(i);
//                dynamicFormCustom.getEditText().setText("");
//            }
        }
    }
}
