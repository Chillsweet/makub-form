package com.jorlek.queqforms.custom.dynamic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.RadioButton;

import com.jorlek.queqforms.AppConfig;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.DynamicRadioGroupBinding;
import com.jorlek.queqforms.databinding.DynamicRadioNewPageBinding;
import com.jorlek.queqforms.main.MainActivity;
import com.jorlek.queqforms.models.request.J_Form_Question_Name_Value;
import com.jorlek.queqforms.models.response.M_formQuestionList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chillsweet on 12/16/2016 AD.
 */

public class DynamicRadioNewPageCustom extends DynamicFieldCustom {

    private Activity mContext;

    private DynamicRadioNewPageBinding binding;

    private M_formQuestionList mFormQuestionList;

    public DynamicRadioNewPageCustom(Context context, M_formQuestionList mFormQuestionList) {
        super(context);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dynamic_radio_new_page, null, false);

        mContext = (Activity) context;

        this.mFormQuestionList = mFormQuestionList;

        setOnClick();

        setTextView(binding.tvDynamic);

        addView(binding.getRoot());
    }

    private void setOnClick() {
        binding.rlLayout.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, DynamicRadioListCustom.class);
            intent.putExtra("mFormQuestionList", mFormQuestionList);
            ((MainActivity) mContext).nextActivityForResult(intent, AppConfig.SELECT_STORE);
        });
    }


}
