package com.jorlek.queqforms.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.Global;
import com.jorlek.queqforms.Prefs;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.custom.DialogCreate;
import com.jorlek.queqforms.databinding.ActivityLoginBinding;
import com.jorlek.queqforms.main.MainActivity;
import com.jorlek.queqforms.models.request.J_reqChangePin;
import com.jorlek.queqforms.models.request.J_reqDefaultInfo;
import com.jorlek.queqforms.models.request.J_reqForgetPin;
import com.jorlek.queqforms.models.request.J_reqLogin;
import com.jorlek.queqforms.models.response.M_reqChangePin;
import com.jorlek.queqforms.models.response.M_reqCheckLock;
import com.jorlek.queqforms.models.response.M_reqDefaultInfo;
import com.jorlek.queqforms.models.response.M_reqForgetPin;
import com.jorlek.queqforms.models.response.M_reqLogin;
import com.jorlek.queqforms.services.AESEngine;
import com.jorlek.queqforms.services.CheckResult;
import com.jorlek.queqforms.services.TokenException;
import com.jorlek.queqforms.helper.writeLog;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by chillsweet on 4/5/2017 AD.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    ActivityLoginBinding binding;

    int countPin = 0;
    String pinCode = "", pinCodeOld = "", pinCodeNew = "", pinEncrypt = "";
    boolean isCreatePin = false, isConfirmPin = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        setOnClick();

        setForgetPin();

        Global.getAboutDevice(this);

    }

    private void setForgetPin() {
        binding.btnForgetPin.setText(Global.setUnderLineText(getString(R.string.login_forget_pin)));
    }

    private void setOnClick() {
        binding.includePinPad.num1.setOnClickListener(this);
        binding.includePinPad.num2.setOnClickListener(this);
        binding.includePinPad.num3.setOnClickListener(this);
        binding.includePinPad.num4.setOnClickListener(this);
        binding.includePinPad.num5.setOnClickListener(this);
        binding.includePinPad.num6.setOnClickListener(this);
        binding.includePinPad.num7.setOnClickListener(this);
        binding.includePinPad.num8.setOnClickListener(this);
        binding.includePinPad.num9.setOnClickListener(this);
        binding.includePinPad.num0.setOnClickListener(this);
        binding.includePinPad.btnDelete.setOnClickListener(this);
        binding.btnForgetPin.setOnClickListener(this);
    }

    private void addPin(String pin) {
        binding.tvIncorrect.setVisibility(View.GONE);
        if (countPin < 4) {
            pinCode = pinCode + pin;
            countPin++;
            switch (countPin) {
                case 1:
                    binding.pin1.setBackgroundResource(R.drawable.round_bg_green);
                    break;
                case 2:
                    binding.pin2.setBackgroundResource(R.drawable.round_bg_green);
                    break;
                case 3:
                    binding.pin3.setBackgroundResource(R.drawable.round_bg_green);
                    break;
                case 4:
                    binding.pin4.setBackgroundResource(R.drawable.round_bg_green);
                    if (Prefs.getRegistration(LoginActivity.this).equals("yes"))
                        callReqDefaultInfo(Global.DEVICE_ID, Prefs.getLanguage(LoginActivity.this), Prefs.getCompanyCode(LoginActivity.this), Prefs.getUserID(LoginActivity.this), true);
                    else
                        callReqLogin(true);
                    break;
            }
        }
    }

    private void addPinCreate(String pin) {
        if (countPin < 4) {
            pinCode = pinCode + pin;
            countPin++;
            switch (countPin) {
                case 1:
                    binding.includeCreatePin.pin1.setText(pin);
                    break;
                case 2:
                    binding.includeCreatePin.pin1.setText("*");
                    binding.includeCreatePin.pin2.setText(pin);
                    break;
                case 3:
                    binding.includeCreatePin.pin2.setText("*");
                    binding.includeCreatePin.pin3.setText(pin);
                    break;
                case 4:
                    binding.includeCreatePin.pin3.setText("*");
                    binding.includeCreatePin.pin4.setText(pin);
                    new Handler().postDelayed(() -> {
                        binding.includeCreatePin.pin4.setText("*");
                        if (!isConfirmPin) setConfirmPin();
                        else setComparePin();
                    }, 300);
                    break;
            }
        }
    }

    private void delPin() {
        if (countPin > 0) {
            countPin--;
            pinCode = pinCode.substring(0, pinCode.length() - 1);
            switch (countPin) {
                case 3:
                    binding.pin4.setBackgroundResource(R.drawable.round_bg_gray);
                    break;
                case 2:
                    binding.pin3.setBackgroundResource(R.drawable.round_bg_gray);
                    break;
                case 1:
                    binding.pin2.setBackgroundResource(R.drawable.round_bg_gray);
                    break;
                case 0:
                    binding.pin1.setBackgroundResource(R.drawable.round_bg_gray);
                    break;
            }
        }
    }

    private void delPinCreate() {
        if (countPin > 0) {
            countPin--;
            pinCode = pinCode.substring(0, pinCode.length() - 1);
            switch (countPin) {
                case 3:
                    binding.includeCreatePin.pin4.setText("");
                    break;
                case 2:
                    binding.includeCreatePin.pin3.setText("");
                    break;
                case 1:
                    binding.includeCreatePin.pin2.setText("");
                    break;
                case 0:
                    binding.includeCreatePin.pin1.setText("");
                    break;
            }
        }
    }

    private void setCreatePin() {
        countPin = 0;
        pinCodeOld = pinCode;
        pinCode = "";
        binding.llPin.setVisibility(View.INVISIBLE);
        binding.llCreate.setVisibility(View.VISIBLE);
        binding.tvTitle.setText(getString(R.string.login_create));
    }

    private void setConfirmPin() {
        pinCodeNew = pinCode;
        isConfirmPin = true;
        binding.tvTitle.setText(getString(R.string.login_confirm));
        clearPin();
    }

    private void setComparePin() {
        if (pinCodeNew.equals(pinCode)) callReqChangePin(true);
        else clearPin();
    }

    private void clearPin() {
        countPin = 0;
        pinCode = "";
        binding.pin1.setBackgroundResource(R.drawable.round_bg_gray);
        binding.pin2.setBackgroundResource(R.drawable.round_bg_gray);
        binding.pin3.setBackgroundResource(R.drawable.round_bg_gray);
        binding.pin4.setBackgroundResource(R.drawable.round_bg_gray);
        binding.includeCreatePin.pin1.setText("");
        binding.includeCreatePin.pin2.setText("");
        binding.includeCreatePin.pin3.setText("");
        binding.includeCreatePin.pin4.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.num_0:
                if (!isCreatePin) addPin("0");
                else addPinCreate("0");
                break;

            case R.id.num_1:
                if (!isCreatePin) addPin("1");
                else addPinCreate("1");
                break;

            case R.id.num_2:
                if (!isCreatePin) addPin("2");
                else addPinCreate("2");
                break;

            case R.id.num_3:
                if (!isCreatePin) addPin("3");
                else addPinCreate("3");
                break;

            case R.id.num_4:
                if (!isCreatePin) addPin("4");
                else addPinCreate("4");
                break;

            case R.id.num_5:
                if (!isCreatePin) addPin("5");
                else addPinCreate("5");
                break;

            case R.id.num_6:
                if (!isCreatePin) addPin("6");
                else addPinCreate("6");
                break;

            case R.id.num_7:
                if (!isCreatePin) addPin("7");
                else addPinCreate("7");
                break;

            case R.id.num_8:
                if (!isCreatePin) addPin("8");
                else addPinCreate("8");
                break;

            case R.id.num_9:
                if (!isCreatePin) addPin("9");
                else addPinCreate("9");
                break;

            case R.id.btn_delete:
                if (!isCreatePin) delPin();
                else delPinCreate();
                break;

            case R.id.btn_forget_pin:
                callReqForgetPin(true);
                break;
        }
    }

    private String setEncryptPin() {
        try {
            return new AESEngine(Prefs.getKeyAES1(LoginActivity.this)).encrypt(pinCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void callReqDefaultInfo(String deviceId, String language, String companyId, String userId, boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            Prefs.setFireBaseToken(LoginActivity.this, FirebaseInstanceId.getInstance().getToken());
            J_reqDefaultInfo jReqDefaultInfo = new J_reqDefaultInfo(deviceId, Prefs.getFireBaseToken(this), language);
            Observable<M_reqDefaultInfo> callReqDefaultInfoApi = services.callApi.reqDefaultInfo(companyId, userId, jReqDefaultInfo);
            callReqDefaultInfoApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqDefaultInfo>() {
                        @Override
                        public void onNext(M_reqDefaultInfo mReqDefaultInfo) {
                            if (mReqDefaultInfo != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqDefaultInfo.getResult_code())) {
                                        Prefs.setCompanyCode(LoginActivity.this, companyId);
                                        Prefs.setUserID(LoginActivity.this, userId);
                                        Prefs.setDeviceID(LoginActivity.this, mReqDefaultInfo.getDevice_id());
                                        Prefs.setDeviceCode(LoginActivity.this, mReqDefaultInfo.getDevice_code());
                                        Prefs.setKeyAES1(LoginActivity.this, mReqDefaultInfo.getKey_aes());
                                    }
                                    else {

                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(LoginActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(LoginActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqDefaultInfo", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            callReqLogin(false);
                        }
                    });
        }
    }

    private void callReqLogin(boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            Observable.fromCallable(this::setEncryptPin)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(result -> {
                        pinEncrypt = result;
                        J_reqLogin jReqLogin = new J_reqLogin(Prefs.getDeviceID(LoginActivity.this), Prefs.getFireBaseToken(LoginActivity.this), 1, Global.DEVICE_OS, APP_VERSION, Global.DEVICE_MODEL + " " + Global.DEVICE_BRAND);
                        Observable<M_reqLogin> callReqLoginApi = services.callApi.reqLogin(Prefs.getCompanyCode(LoginActivity.this), Prefs.getUserID(LoginActivity.this), pinEncrypt, jReqLogin);
                        callReqLoginApi
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new DisposableObserver<M_reqLogin>() {
                                    @Override
                                    public void onNext(M_reqLogin mReqLogin) {
                                        if (mReqLogin != null) {
                                            try {
                                                if (CheckResult.checkSuccess(mReqLogin.getResult_code())) {
                                                    Prefs.setKeyAES2(LoginActivity.this, mReqLogin.getKey_aes());
                                                    Prefs.setToken(LoginActivity.this, mReqLogin.getToken());
                                                    if (mReqLogin.isReset_flag()) {
                                                        isCreatePin = true;
                                                        setCreatePin();
                                                    }
                                                    else {
                                                        startProcess();
                                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        nextActivity(i);
                                                        finish();
                                                    }
                                                }
                                                else {
                                                    binding.tvIncorrect.setVisibility(View.VISIBLE);
                                                    clearPin();
                                                    callReqCheckLock(true);
                                                }
                                            } catch (TokenException e) {
                                                e.printStackTrace();
                                                Global.showTokenExpireDialog(LoginActivity.this);
                                            }
                                        }
                                        else Global.showTimeOutDialog(LoginActivity.this);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        dispose();
                                        services.hideDialog();
                                        writeLog.LogE("reqLogin", e.getMessage());
                                    }

                                    @Override
                                    public void onComplete() {
                                        dispose();
                                        services.hideDialog();
                                    }
                                });
                    })
                    .subscribe();
        }
    }

    private void callReqCheckLock(boolean isShowDialog) {
        if (Global.isNetworkOnline(LoginActivity.this)) {
            services.showDialog(isShowDialog);
            Observable<M_reqCheckLock> callReqCheckLockApi = services.callApi.reqCheckLock(Prefs.getDeviceID(LoginActivity.this));
            callReqCheckLockApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqCheckLock>() {
                        @Override
                        public void onNext(M_reqCheckLock mReqCheckLock) {
                            if (mReqCheckLock != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqCheckLock.getResult_code())) {
                                        if (mReqCheckLock.isLock_flag()) {
                                            Intent i = new Intent(LoginActivity.this, CheckLockActivity.class);
                                            i.putExtra("msg", mReqCheckLock.getLock_message());
                                            i.putExtra("second", mReqCheckLock.getLock_second());
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            nextActivity(i);
                                            finish();
                                        }
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(LoginActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(LoginActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqCheckLock", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

    private String[] setEncryptPinNew() {
        String[] result = new String[2];
        try {
            result[0] = new AESEngine(Prefs.getKeyAES2(LoginActivity.this)).encrypt(pinCodeOld);
            result[1] = new AESEngine(Prefs.getKeyAES2(LoginActivity.this)).encrypt(pinCodeNew);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void callReqChangePin(boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            Observable.fromCallable(this::setEncryptPinNew)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(result -> {
                        J_reqChangePin jReqChangePin = new J_reqChangePin(result[0], result[1]);
                        Observable<M_reqChangePin> callReqChangePinApi = services.callApi.reqChangePin(Prefs.getToken(LoginActivity.this), jReqChangePin);
                        callReqChangePinApi
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new DisposableObserver<M_reqChangePin>() {
                                    @Override
                                    public void onNext(M_reqChangePin mReqChangePin) {
                                        if (mReqChangePin != null) {
                                            try {
                                                if (CheckResult.checkSuccess(mReqChangePin.getResult_code())) {
                                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                                    nextActivity(i);
                                                    finish();
                                                }
                                            } catch (TokenException e) {
                                                e.printStackTrace();
                                                Global.showTokenExpireDialog(LoginActivity.this);
                                            }
                                        }
                                        else Global.showTimeOutDialog(LoginActivity.this);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        dispose();
                                        services.hideDialog();
                                        writeLog.LogE("callReqChangePin", e.getMessage());
                                    }

                                    @Override
                                    public void onComplete() {
                                        dispose();
                                        services.hideDialog();
                                    }
                                });
                    })
                    .subscribe();
        }
    }

    private void callReqForgetPin(boolean isShowDialog) {
        if (Global.isNetworkOnline(this)) {
            services.showDialog(isShowDialog);
            J_reqForgetPin jReqForgetPin = new J_reqForgetPin(Prefs.getCompanyCode(LoginActivity.this), Prefs.getUserID(LoginActivity.this), Global.DEVICE_ID);
            Observable<M_reqForgetPin> callReqForgetPinApi = services.callApi.reqForgetPin(Prefs.getToken(LoginActivity.this), jReqForgetPin);
            callReqForgetPinApi
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<M_reqForgetPin>() {
                        @Override
                        public void onNext(M_reqForgetPin mReqForgetPin) {
                            if (mReqForgetPin != null) {
                                try {
                                    if (CheckResult.checkSuccess(mReqForgetPin.getResult_code())) {
                                        new DialogCreate(LoginActivity.this).AlertError(mReqForgetPin.getMessage(), getString(R.string.check_expire_button), (dialog, which) -> {
                                        });
                                    }
                                } catch (TokenException e) {
                                    e.printStackTrace();
                                    Global.showTokenExpireDialog(LoginActivity.this);
                                }
                            }
                            else Global.showTimeOutDialog(LoginActivity.this);
                        }

                        @Override
                        public void onError(Throwable e) {
                            dispose();
                            services.hideDialog();
                            writeLog.LogE("callReqForgetPin", e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                            services.hideDialog();
                        }
                    });
        }
    }

}
