package com.jorlek.queqforms.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;


import com.jorlek.queqforms.BaseActivity;
import com.jorlek.queqforms.R;
import com.jorlek.queqforms.databinding.ActivityCheckLockBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ChillSweet on 2/17/2016.
 */
public class CheckLockActivity extends BaseActivity {

    private ActivityCheckLockBinding binding;

    private String msg;
    private int second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_lock);

        getBundleExtras();

        setCountDown(second * 1000);

    }

    private void getBundleExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            msg = extras.getString("msg");
            second = extras.getInt("second");
        }
    }

    private void setCountDown(final int second) {

        binding.tvLockMsg.setText(msg);

        new CountDownTimer(second, 1000) {

            public void onTick(long millisUntilFinished) {
                TimeZone tz = TimeZone.getTimeZone("UTC");
                SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                df.setTimeZone(tz);
                String time = df.format(new Date(millisUntilFinished));
                binding.tvLockSecond.setText(time);
            }

            public void onFinish() {
                Intent i = new Intent(CheckLockActivity.this, LoginActivity.class);
                nextActivity(i);
                finish();
            }

        }.start();

    }

}
